#pragma once
#include <exception>
#include <string>
#include "InterThreadMsg.h"
class GeneralException :public std::exception {
	std::string _errorMsg = "";
public:
	GeneralException(std::string error)
	{
		_errorMsg = error;
	};
	std::string getError() { return _errorMsg; };
};

class NodeCrashException: public std::exception
{
};

class LackingBytes : public std::exception
{
public:
	const char* what() const {
		return "Not enough bytes";
	}
};

class UninitiatedValue : public std::exception
{
public:
	const char* what() const {
		return "Error: value not initialized";
	};
};

class CommunicationException : public std::exception {
	int _code;
	std::string _errorMsg;
public:
	CommunicationException(std::string errorMsg, int errorCode)
	{
		_errorMsg = errorMsg;
		_code = errorCode;
	};
	std::string getErrorMsg() { return _errorMsg; };
	int getErrorCode() { return _code; };
	const char* what() const {
		return (_errorMsg + "\nError code: " + std::to_string(_code)).c_str();
	};
};

//thrown if connectToServer was not called in Communicator.
class UninitiatedConnection :public UninitiatedValue {
};


class InterThreadUninitiatedConnection : public UninitiatedConnection, public InterThreadMsg {
public:
	virtual std::string getType() { return "InterThreadUninitiatedConnection"; };
	virtual ~InterThreadUninitiatedConnection() {};
};


class InterThreadCommunicationException : public InterThreadMsg {
	CommunicationException exc;
public:
	InterThreadCommunicationException(CommunicationException cmExc)
		:exc(cmExc.getErrorMsg(), cmExc.getErrorCode())
	{

	}
	std::string getErrorMsg() { return exc.getErrorMsg(); };
	int getErrorCode() { return exc.getErrorCode(); };
	virtual ~InterThreadCommunicationException() {};
	virtual std::string getType() { return "InterThreadCommunicationException"; };
};

class TimeOut :public std::exception {

};


class InvalidMsgLen : public GeneralException {
public:
	InvalidMsgLen(std::string error)
		:GeneralException(error)
	{
	}
};

class ShutdownException : public std::exception {
public:
	ShutdownException() {};
	std::string getError() { return "The function was shutdown before it could finish running"; };
};


class SqliteFailureException :public GeneralException {
public:
	SqliteFailureException(std::string error)
		:GeneralException(error)
	{
	}
};

class CloseDBException :public std::exception {
	std::string errorMsg = "";
public:
	CloseDBException()
	{
		errorMsg = "You must open the db before accessing data!";
	};
	CloseDBException(std::string error)
	{
		errorMsg = error;
	};

	std::string getError() { return errorMsg; };
};

class KeyExchangeError : public GeneralException {
public:
	KeyExchangeError(): GeneralException("Key exchange error")
	{

	}
};