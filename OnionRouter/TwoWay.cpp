#include "TwoWay.h"

void TwoWay::shutdownClass(bool prevCalled, bool nextCalled)
{
	std::unique_lock<std::mutex> endClassLck(_endClassMtx);
	if (!_endClass) {
		std::unique_lock<std::mutex> finishShutdownLck(_finishShutdownMtx);
		_endClass = true;
		endClassLck.unlock();
		_nextNode.shutdownClass();
		_prevNode->shutdownClass();//shuts down previous and next
		if (!prevCalled) {//if the previous listening thread called, don't wait for it to join
			_handleRecvFromPrevThread->join();
			delete _handleRecvFromPrevThread;
			_handleRecvFromPrevThread = nullptr;
		}
		if (!nextCalled) {//if the next listening thread called, don't wait for it to join
			_handleRecvFromNextThread->join();
			delete _handleRecvFromNextThread;
			_handleRecvFromNextThread = nullptr;
		}
		_exitCall();//removes the node from the node maps in ConnectionHandler
		delete _prevNode;
	}
	
}

void TwoWay::handleRecvFromPrevThread()
{
	std::vector<byte> receivedMsg;
	std::array<byte, 2> flags = {};
	int16_t length = 0;
	while (_prevNode->recvOne(receivedMsg, flags, length)) {
		try {
			std::cout << std::string("FORWARDED MSG ") + std::to_string((int)_nodeName) + " " + std::to_string((int)flags[MSG_TYPE]) +" " + std::to_string(receivedMsg.size()) + "\n";
			_nextNode.send(receivedMsg, flags, length);
			length = 0;
		}
		catch (ShutdownException e) {
			std::cout << "SHUTDOWN EXCEPTION\n";
			break;
		}
		catch (NodeCrashException e) {
			std::cout << "NODE CRASH EXCEPTION\n";
			continue;
		}
	}
	shutdownClass(true, false);
}

void TwoWay::handleRecvFromNextThread()
{
	UnprocessedMsg msg;
	while (!_endClass) {
		bool gotMsg = getMsg(msg);
		if (msg.getFlag() == MSG_TYPES::REGULAR) {
			if (_nodeName == NODE_NAMES::GUARD_NODE) {
				int i = 0;
			}
			else if (_nodeName == NODE_NAMES::MID_NODE) {
				int i = 0;
			}
		}
		if (_endClass) {
			break;
		}
		if (!gotMsg) {//if the next node crashed, returns a node crash to OC
			std::cout << std::string("NODE CRASH ") + std::to_string((int)_nodeName) + "\n";
			_prevNode->sendMsg(std::vector<byte>(), (byte)MSG_TYPE_MEANING::NODE_CRASH_MSG);
			break;
		}
	
		if (msg.getFlag() == MSG_TYPES::PATH_CLOSURE) {//if the msg is a path closure msg, shut down. TODO ensure msg is from OC
			std::cout << std::string("PATH CLOSURE MSG ARRIVED ") + std::to_string((int)_nodeName) + "\n";
			if (_prevNode->getNodeName() == NODE_NAMES::MID_NODE) {//if its mid, pass the msg back to guard. If its guard, no need to send msg to OC
				_prevNode->sendMsg(msg.getMsg(), (byte)msg.getFlag(), msg.getOrigin());
			}
			break;
		}
		else {//backwars the msg if its a regular msg.
			std::cout << std::string("BACKWARDED MSG ") + std::to_string((int)_nodeName) + " " + std::to_string((int)msg.getFlag()) + " " + std::to_string(msg.getMsg().size()) + "\n";
			_prevNode->sendMsg(msg.getMsg(), (byte)msg.getFlag(), msg.getOrigin(), msg.getLength());
		}
	}
	shutdownClass(false, true);
}

bool TwoWay::getMsg(UnprocessedMsg& um)
{
	try {
		um = _nextNode.recv();
		return true;
	}
	catch (ShutdownException) {
		return false;
	}
	catch (NodeCrashException) {
		return false;
	}
}

TwoWay::TwoWay(ConnectionLeft* prevNode, std::vector<byte> nextNodeIP, std::function<void()> exitCall): _nextNode(nextNodeIP, (int)prevNode->getNodeName()), _prevNode(prevNode), _exitCall(exitCall), _nodeName(_prevNode->getNodeName())
{
	_prevNode->sendMsg({ 1 }, (byte)MSG_TYPE_MEANING::PATH_GENERATION_MSG);
	_handleRecvFromPrevThread = new std::thread(&TwoWay::handleRecvFromPrevThread, this);
	_handleRecvFromNextThread = new std::thread(&TwoWay::handleRecvFromNextThread, this);
}

TwoWay::~TwoWay()
{
	shutdownClass();
	std::unique_lock<std::mutex> finishShutdownLck(_finishShutdownMtx);//wait for shutdownClass to finish executing in the process where it enters the if statement
	finishShutdownLck.unlock();
	if (_handleRecvFromPrevThread != nullptr) {
		_handleRecvFromPrevThread->join();
		delete _handleRecvFromPrevThread;
	}
	if (_handleRecvFromNextThread != nullptr) {
		_handleRecvFromNextThread->join();
		delete _handleRecvFromNextThread;
	}
}

void TwoWay::switchSocket(SOCKET s)
{
	_prevNode->newSocket(s);//switches to a new socket
}
