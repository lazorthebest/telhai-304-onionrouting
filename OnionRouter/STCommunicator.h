#pragma once
#include <condition_variable>
#include "ICommunicator.h"
#define BUFF_LEN 1024
#define LEN_DESCRIPTION_LEN 2
#define DS_PORT 43984

class STCommunicator:public ICommunicator
{
	WSAEVENT _readEvent = nullptr;
	std::condition_variable _notifyCloseCV;
	std::mutex _notifyCloseMtx;
	bool _notifyCloseBool = false;
public:
	virtual void closeCommunication();
	//THROWS Communication Exception
	virtual void connectToServer();
	void setCommunicationDetails(std::vector<byte> ip);
	virtual void sendToServer( std::vector<byte> toSend, byte flags = 0) ;
	//will not return the  bytes describing the length of the full message.
	//Timeout per recv performed in the function. total timeout can be greater.
	//THROWS TimeOut if timeout has been reached.
	//THROWS CommunicationException
	std::vector<byte> receiveFromServer(unsigned int timeoutMilli = WSA_INFINITE);
	~STCommunicator();
};

