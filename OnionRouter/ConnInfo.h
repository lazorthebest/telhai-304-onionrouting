#pragma once
#include <vector>
typedef unsigned char byte;

struct ConnInfo {
	std::vector<byte> IP;
	unsigned short port;
	friend bool operator<(const ConnInfo& one, const ConnInfo& two);
};

