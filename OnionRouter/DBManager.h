#pragma once
#include <string>
#include <exception>
#include <mutex>
#include <chrono>
#include <vector>
#include <algorithm>
#include <random>
#include <thread>
#include <io.h>
#include "sqlite3.h"
#include "Utilities.h"
#include "Exceptions.h"

#define NOT_EXIST -1

class DBManager {
	std::mutex _openDBMtx, _updatedDataMtx, _guardDataMtx;//locks when opening the database
	std::condition_variable _stopThreadCv;
	sqlite3* _m_db;

	void runLine(std::string command);//THROWS SqliteFailureExcpetion. Runs the line entered.
	DBManager();//THROWS ClosedDBException and SqliteFailureExcpetion
	~DBManager();
public:
	DBManager(DBManager const&) = delete;
	void operator=(DBManager const&) = delete;
	static DBManager& instance();//gets the singleton

	std::vector<byte> getDSIP();//THROWS SqliteFailureExcpetion. Gets a random data server IP
	void addDSIP(std::string DSIP);//THROWS SqliteFailureExcpetion. Adds a data server IP
	void removeDSIP(std::string DSIP);//THROWS SqliteFailureExcpetion.
};

int callback(void* data, int argc, char** argv, char** azColName);//callback to be used with sqlite
