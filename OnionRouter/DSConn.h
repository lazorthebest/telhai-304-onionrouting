#pragma once
#include<iostream>
#include <atomic>
#include <mutex>
#include <unordered_map>
#include <fstream>
#include <vector>
#include "STCommunicator.h"
#include "EncryptionUtils.h"
#define IP_STR_LEN 15
#define DS_AMOUNT 9
#define DS_PORT 43984
#define SEC_IN_MILLI 1000
#define SUCCESS 1
#define FAIL 0

enum class DS_MSGS {//no two flags can be on at once
	GIVE_NODES = 0,
	GET_NEW_NODE,
	REPORT_NODE,
	GET_GUARD_NODE,
	NODE_ALIVE
};

#define IP_LEN 4
#define TOKEN_LEN 16
typedef struct PathIps {
	std::vector<byte> guardIp;
	std::vector<byte> midIp;
	std::vector<byte> exitIp;
}PathIps;

typedef std::vector<byte> byteVec;
class DSConn
{
	byteVec _key;

	std::mutex& _tokensMtx;
	std::condition_variable& _tokensCv;
	std::unordered_map<uint64_t, uint64_t>& _tokens;

	std::thread* _recvThread;
	std::atomic<bool> _shutdown = false;

	STCommunicator _cm;

	std::ifstream _inputFile = std::ifstream("DSs.data");
	byteVec getNextDS();

	//if the DSIPs parameter is given, then instead of using the db to get IPs of data servers, the funciton willl use the vector supplied.
	void reconnectDS();
	void makeInitialCommunicationAndKeyExchange();
	byteVec sendRecvDS(byteVec msg);

	void recvThread();
public:
	//inits the class and NOTIFIES the DS we are on.
	DSConn(std::unordered_map<uint64_t, uint64_t>& tokens, std::mutex& tokensMtx, std::condition_variable& tokensCv);
	~DSConn();

};
