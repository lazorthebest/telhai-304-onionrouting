#pragma once

enum class ConnectionCreationReturnValue {
	SUCCESS,
	SOCKET_INITIALIZATION_FAILED, 
	INVALID_IP,
	CONNECTION_FAILED
};
