#include "Communicator.h"

bool Communicator::isLastPart(std::vector<byte> msg)
{
	return (bool)(Utilities::isolateBit(msg[(byte)HEADERS::BITS_HEADER], (byte)BITS_HEADER::FINAL_FRAGMENT_FLAG));
}

inline NODE_NAMES Communicator::getOrigin(std::vector<byte> msg)
{
	if (Utilities::isolateBit(msg[(byte)HEADERS::BITS_HEADER], (byte)BITS_HEADER::IS_GUARD)){
		return NODE_NAMES::GUARD_NODE;
	}
	else if (Utilities::isolateBit(msg[(byte)HEADERS::BITS_HEADER], (byte)BITS_HEADER::IS_MID)) {
		return NODE_NAMES::MID_NODE;
	}
	else {
		return NODE_NAMES::EXIT_NODE;
	}
}

Communicator::Communicator(std::mutex& outQueueMtx, std::condition_variable& outQueueCv, std::queue<InterThreadMsg*>& outQueue)
	: _outQueueMtx(outQueueMtx), _outQueue(outQueue), _outQueueCv(outQueueCv)
{
	_recvThread = nullptr;
}
Communicator::~Communicator()
{
	if (_connectSocket != INVALID_SOCKET) {
		_toShutDown = true;
		closesocket(_connectSocket);
		if (_recvThread != nullptr) {
			_recvThread->join();//waits here
			delete _recvThread;
			_recvThread = nullptr;
		}
		WSACleanup();
	}
	InterThreadMsg* toDelete;
	std::unique_lock<std::mutex> outQueueLck(_outQueueMtx);
	while (_outQueue.size() != 0) {
		toDelete = _outQueue.front();
		delete toDelete;
		_outQueue.pop();
	}
}

void Communicator::connectToServer()
{
	ICommunicator::connectToServer();
	_recvThread = new std::thread(&Communicator::receiveThread, this);
}

void Communicator::setCommunicationDetails(std::vector<byte> ip)
{	
	ICommunicator::setCommunicationDetails(ip, DEFAULT_PORT);
}


void Communicator::sendToServer(std::vector<byte> toSend)
{
	if (toSend.size() % MSG_LEN != 0) {
		throw LackingBytes();
	}
	unsigned short  maxPart = ceil(((float)toSend.size()) / MSG_LEN);
	int iResult = 0;
	char* arrPtr = nullptr;
	for (unsigned short i = 0; i < maxPart; i++) {
		std::vector<byte> temp = std::vector<byte>(toSend.begin() + MSG_LEN * i, toSend.begin() + MSG_LEN * i + MSG_LEN);
		arrPtr = (char*)temp.data();
		iResult = send(_connectSocket, arrPtr, MSG_LEN, 0);
		if (iResult == SOCKET_ERROR) {
			closesocket(_connectSocket);
			WSACleanup();
			throw(CommunicationException("send failed", WSAGetLastError()));
		}
	}
}

std::vector<byte> Communicator::receiveFromServer()
{
	if (INVALID_SOCKET == _connectSocket)
	{
		throw (UninitiatedConnection());
	}
	char buffer[MSG_LEN] = { 0 };
	short bytesReadTotal = 0;
	short bytesRead = 0;
	bytesRead = recv(_connectSocket, buffer + bytesReadTotal, MSG_LEN - bytesReadTotal, 0);//we don't want to recv bytes of a different msg.
	bytesReadTotal += bytesRead;
	if (bytesRead <= 0) {
		throw(CommunicationException("The connection with the server has been lost.", WSAGetLastError()));
	}
	return std::vector<byte>(buffer, buffer + MSG_LEN);
}


void Communicator::receiveThread()
{
	std::vector<byte> tempMsg;
	std::vector<byte> msg;
	std::vector<std::vector<byte>> toSave;
	char buffer[MSG_LEN] = { 0 };
	unsigned short bytesRead = 0;
	unsigned short bytesReadTotal = 0;
	unsigned short msgId = 0;
	NODE_NAMES origin= NODE_NAMES::NO_NODE;
	MSG_TYPES type = MSG_TYPES::REGULAR;
	NODE_NAMES tempOrigin = NODE_NAMES::NO_NODE;
	bool tempIsLastPart = false;
	int16_t lastLen = 0, actualLength, lengthBeforeLast = 0;
	std::unique_lock<std::mutex> outQueueLck(_outQueueMtx, std::defer_lock);
	int rv = 0;
	WSAEVENT readEvent = WSACreateEvent();
	rv = WSAEventSelect(_connectSocket, readEvent, FD_READ|FD_CLOSE);//If socket closes, a recv will be attempted, will fail, and that will raise an error.

	while (!_toShutDown){
		try {
			rv = WSAWaitForMultipleEvents(1, &readEvent,FALSE,WSA_INFINITE,TRUE);
			if (WSAResetEvent(readEvent) == FALSE) {
				throw(CommunicationException("Communication error!", WSAGetLastError()));
			}
			if (WSA_WAIT_EVENT_0== rv) {
				bytesRead = recv(_connectSocket, buffer+ bytesReadTotal, MSG_LEN- bytesReadTotal, 0);//we don't want to recv bytes of a different msg.
				bytesReadTotal += bytesRead;
				if (bytesRead <= 0) {
					throw(CommunicationException("The connection with the server has been lost.", WSAGetLastError()));
				}
				if (bytesReadTotal < MSG_LEN) {//if no recevied an entire fragment yet.
					continue;
				}
				else {
					bytesReadTotal = 0;
				}
				tempMsg = std::vector<byte>((byte*)buffer, (byte*)buffer + MSG_LEN);
				tempIsLastPart = isLastPart(tempMsg);
				//if a node crashsed, a new node may send a crash packet
				if (origin == NODE_NAMES::NO_NODE) {//first msg in a sequence.
					origin = getOrigin(tempMsg);
					type=(MSG_TYPES) tempMsg[(byte)HEADERS::MSG_TYPE_HEADER];
					actualLength = Utilities::bigEndianBytesToIntegerType<int16_t>(
						std::vector<byte>(tempMsg.begin() + (byte)HEADERS::LAST_FRAG_LEN, tempMsg.end()));
					lengthBeforeLast = MSG_LEN - HEADERS_LEN_START;

					if (!tempIsLastPart)
					{
						msg.insert(msg.begin(), tempMsg.begin(), tempMsg.end());
					}
					else {
						lastLen = CryptoPP::AES::BLOCKSIZE - (actualLength - HEADERS_LEN_START) % CryptoPP::AES::BLOCKSIZE;
						if (lastLen == CryptoPP::AES::BLOCKSIZE) {
							lastLen = 0;
						}
						lastLen += actualLength;
						msg.insert(msg.begin(), tempMsg.begin(), tempMsg.begin() + lastLen);
					}
				}
				else if (origin != (tempOrigin = getOrigin(tempMsg))) {//means this is a server crashmsg from a new node
					origin = tempOrigin;
					lastLen = Utilities::bigEndianBytesToIntegerType<unsigned short>(
						std::vector<byte>(tempMsg.begin() + (byte)HEADERS::LAST_FRAG_LEN, tempMsg.end()));
					type = (MSG_TYPES)tempMsg[(byte)HEADERS::MSG_TYPE_HEADER];
					msg = std::vector<byte>(tempMsg.begin(), tempMsg.begin() + lastLen);
				}
				else if(tempIsLastPart) {//ending of a sequence
					lastLen = CryptoPP::AES::BLOCKSIZE - (lengthBeforeLast + actualLength - HEADERS_LEN_LATE) % CryptoPP::AES::BLOCKSIZE;
					lastLen += actualLength;
					msg.insert(msg.end(), tempMsg.begin(), tempMsg.begin() + lastLen);
				}
				else {//continuation && not ending of a sequence
					msg.insert(msg.end(), tempMsg.begin(), tempMsg.end());
					lengthBeforeLast += MSG_LEN - HEADERS_LEN_LATE;
					if (msg[1] == (byte)MSG_TYPES::PATH_CLOSURE) {
						std::cout << "GOT CORRECT MSG\n";
					}
				}
				if (tempIsLastPart) {
					outQueueLck.lock();
					_outQueue.push(new UnprocessedMsg(origin, type, msg, actualLength));
					outQueueLck.unlock();
					_outQueueCv.notify_all();
					toSave.push_back(msg);
					msg.resize(0);
					origin = NODE_NAMES::NO_NODE;
				}
			}
		}
		catch (CommunicationException cmExc) {
			outQueueLck.lock();
			_outQueue.push(new InterThreadCommunicationException(cmExc));
			outQueueLck.unlock();
			break;
		}
	}
	WSACloseEvent(readEvent);
}



void Communicator::closeCommunication()
{
	if (_connectSocket != INVALID_SOCKET) {
		if (_recvThread != nullptr) {
			_toShutDown = true;
			_recvThread->join();
			delete _recvThread;
			_recvThread = nullptr;
		}
		ICommunicator::closeCommunication();
		_toShutDown = false;
		_connectSocket = INVALID_SOCKET;
	}
}

