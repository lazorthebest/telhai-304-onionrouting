#pragma once
#include <winsock2.h>
#include <exception>
#include <ws2tcpip.h>
#include <string>
#include <string.h>
#include <vector>
#include<iostream>
#include <algorithm>
#include "Exceptions.h"
#include "Utilities.h"
#define PORT_UNSET -1
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")

class ICommunicator
{
protected:
	SOCKET _connectSocket = INVALID_SOCKET;
	int _port = PORT_UNSET;
	std::vector<byte> _ipAddr;
public:
	void setCommunicationDetails(std::vector<byte> ip, unsigned short port);
	//THROWS CommunicationException!
	virtual void connectToServer();//used to connect to the server. must be used before send and recieve.
	//call closeCommunication to close the connection. Always call it if you want to exit peacefully.
	virtual void closeCommunication();
};