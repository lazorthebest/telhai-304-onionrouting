#pragma once
#include <iostream>
#include <atomic>
#include "Exceptions.h"
#include "UnprocessedMsg.h"
#include "Communicator.h"

#define SECS_TO_WAIT 5
#define SECS_TO_WAIT_QUICK 1
#define ID_LEN 8
#define END_CONN_FLAG_SHIFT 2
#define REGULAR_MSG_VALUE 0//except the final flag msg of course
#define NO_TIMEOUT -1
#define ONE_ACTIVE_THREAD 1
#define NUM_OF_THREADS 3

enum class THREADS {
	ENSURE_GUARD_CONN_THREAD,
	RECV_THREAD,
	ENSURE_INTERNET_CONN_THREAD
};
typedef unsigned char byte;
typedef std::vector<byte> byteVec;
typedef std::unique_lock<std::mutex> unqLck;
class PathManager;


class ConnectionRight {

	std::mutex _threadsMtx;
	std::vector<std::thread*> _threads = std::vector<std::thread*>({ nullptr, nullptr });

	std::atomic<bool> _toShutDownThreads = false;//whether to shut down threads. Turned on also when the next node crashes.
	std::atomic<bool> _hasNodeCrashed = false;
	std::mutex _cmSendMtx;
	Communicator _cm;

	std::mutex _recvQueueMtx;
	std::condition_variable _recvQueueCv;
	std::queue<InterThreadMsg* > _recvQueue;
	
	std::mutex _recvToForwardMtx;
	std::condition_variable _recvToForwardCV;
	std::queue<UnprocessedMsg> _recvToForward;

	std::mutex _ensureNodeConnMtx;
	bool _ensureNodeConnBool = false;
	std::condition_variable _ensureNodeConnCV;

	std::atomic<unsigned int> _checkNextNodeMsgsCount;
	int _temp;


	void receiveThread();
	void ensureGuardNodeConnThread();

public:
	//responsible for called the send of cm.
	//THROWS NodeCrashException!
	//THROWS ShutdownExcpetion!
	//THROWS CommunicationException
	void send(byteVec msg, std::array<byte, 2> flags, int16_t length);
	//returns one message at a time 
	//BLOCKS if no messages exist
	//first bte of the byteVec will be the flags.
	//THROWS NodeCrashException!
	//THROWS ShutdownException!
	//THROWS TimeOut!
	UnprocessedMsg recv(int timeoutMilliSec = NO_TIMEOUT);
	//must be called to close the connections and exit recv functions.
	void shutdownClass();
	//THROWS CommunicationException!
	ConnectionRight(byteVec ip, int temp = 0);
};

