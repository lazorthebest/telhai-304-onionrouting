#pragma once
#include <vector>
#include "Exceptions.h"
#include "InterThreadMsg.h"
#include "Communicator.h"

enum class NODE_NAMES;
enum class MSG_TYPES;

typedef unsigned char byte;
class UnprocessedMsg :
	public InterThreadMsg
{
	MSG_TYPES _flag;
	NODE_NAMES _origin;
	std::vector<byte> _msg;
	int16_t _length;
	bool _isInit;
public:
	UnprocessedMsg() {
		_isInit = false;
	};

	UnprocessedMsg(NODE_NAMES origin, MSG_TYPES flag, std::vector<byte> msg, int16_t length) {
		_isInit = true;
		_msg = msg;
		_origin = origin;
		_flag = flag;
		_length = length;
	}

	std::vector<byte> getMsg() {
		if (!_isInit) {
			throw UninitiatedValue();
		}
		return _msg;
	}
	MSG_TYPES getFlag() {
		if (!_isInit) {
			throw UninitiatedValue();
		}
		return _flag;
	}

	NODE_NAMES getOrigin() {
		if (!_isInit) {
			throw UninitiatedValue();
		}
		return _origin;
	}

	int16_t getLength() {
		if (!_isInit) {
			throw UninitiatedValue();
		}
		return _length;
	}
	virtual ~UnprocessedMsg() {};
	virtual std::string getType() { return "UnprocessedMsg"; };
};
