#include "ConnectionLeft.h"

bool ConnectionLeft::keyExchange()
{
	std::vector<byte> privateKey = EncryptionUtils::generateRand(PRIVATE_EXPONENT_LEN), msgReceived, longKey;
	std::array<byte, 2> flags;

	if(!recvOne(msgReceived, flags)) {
		return false;
	}

	bool isMid = Utilities::getBit(flags[OTHER_FLAGS], (byte)FLAGS::IS_MID), isGuard = Utilities::getBit(flags[OTHER_FLAGS], (byte)FLAGS::IS_GUARD);
	if (isGuard) {
		setNodeName(NODE_NAMES::GUARD_NODE);
	}
	else if (isMid) {
		setNodeName(NODE_NAMES::MID_NODE);
	}
	else {
		setNodeName(NODE_NAMES::EXIT_NODE);
	}

	if (!sendMsg(EncryptionUtils::power2(privateKey.data()), (byte)MSG_TYPE_MEANING::KEY_EXCHANGE_MSG)) {
		return false;
	}
	longKey = EncryptionUtils::power(msgReceived.data(), privateKey.data());
	_key = EncryptionUtils::generateKey(longKey.data());
	return true;
}

std::vector<byte> ConnectionLeft::encrypt(std::vector<byte> msg)
{
	if (msg.size() == 0) {
		return msg;
	}
	std::vector<byte> iv = EncryptionUtils::generateRand(CryptoPP::AES::BLOCKSIZE), toReturn;//generating the initial vector
	msg = EncryptionUtils::encrypt(msg, _key.data(), iv.data());
	
	toReturn = iv;
	toReturn.insert(toReturn.end(), msg.begin(), msg.end());
	return toReturn;
}

std::vector<byte> ConnectionLeft::decrypt(std::vector<byte> cipherText)
{
	if (cipherText.size() == 0) {
		return cipherText;
	}
	std::vector<byte> iv = std::vector<byte>(cipherText.begin(), cipherText.begin() + CryptoPP::AES::BLOCKSIZE);//getting the initial vector
	cipherText = std::vector<byte>(cipherText.begin() + CryptoPP::AES::BLOCKSIZE, cipherText.end());//getting the encrypted data
	return EncryptionUtils::decrypt(cipherText, _key.data(), iv.data());//return the decryption of the msg
}

bool ConnectionLeft::safeRecv(SOCKET s, char* buffer, size_t len)
{
	int rv = WSA_WAIT_TIMEOUT;
	int numRecv = 0;
	while (!_endClass) {
		rv = WSAWaitForMultipleEvents(1, (HANDLE*)&_readEvent, FALSE, SECOND / 10, TRUE);
		if (rv == WSA_WAIT_TIMEOUT || rv == WSA_WAIT_IO_COMPLETION) {
			continue;
		}
		if (WSAResetEvent(_readEvent) == FALSE) {
			throw(CommunicationException("Communication error!", WSAGetLastError()));
		}
		if (WSA_WAIT_EVENT_0 == rv) {
			while (numRecv < HEADER_SIZE + DATA_SIZE) {
				rv = recv(_connectSocket, buffer + numRecv, (int)len, 0);
				if (rv == SOCKET_ERROR) {
					throw(CommunicationException("Communication error!", WSAGetLastError()));
				}
				else if (rv == 0) {
					return false;
				}
				numRecv += rv;
			}
			return true;
		}
	}

	return 0;
}

void ConnectionLeft::waitInternetIssue()
{
	std::mutex mtx;
	std::unique_lock<std::mutex> lck(mtx);
	bool isInternetConnOk = false;
	while (!isInternetConnOk) {
		isInternetConnOk = Utilities::isIPV4InternetConnectionOk();
		if (_endClassCV.wait_for(lck, std::chrono::milliseconds(SECOND / 10), [this] { return (bool)_endClass; })) {
			break;
		}
	}
}

void ConnectionLeft::recvMsgsThread()
{
	std::unique_lock<std::mutex> connectionBrokenLck(_connectionBrokenMtx, std::defer_lock);
	char buffer[MSG_LEN];
	while (!_endClass) {
		try {
			if (!safeRecv(_connectSocket, buffer, MSG_LEN)) {
				throw CommunicationException("Connection ended", WSAECONNRESET);
			}
		}
		catch (CommunicationException e) {

			if (_endClass) {
				std::unique_lock<std::mutex> connectionEndedLck(_connectionEndedMtx);
				_connectionEnded = true;
				connectionEndedLck.unlock();
				_connectionEndedCV.notify_one();
				return;
			}
			else if (e.getErrorCode() == WSAECONNRESET) {
				unsigned int timeToWait = 0;
				if (_nodeName == NODE_NAMES::EXIT_NODE) {
					timeToWait = WAIT_IF_EXIT;
				}
				else if (_nodeName == NODE_NAMES::MID_NODE) {
					timeToWait = WAIT_IF_MID;
				}
				connectionBrokenLck.lock();
				//if (_connectionBrokenCV.wait_for(connectionBrokenLck, std::chrono::seconds(timeToWait), [this] { return _connectionBroken || _endClass; })) {
					connectionBrokenLck.unlock();//TODO making it always enter because switching connections is not implemented yet
					shutdownClass(true, false, true);
					return;
				//}
				//else {
					connectionBrokenLck.unlock();
					continue;
				//}
			}
			else {
				waitInternetIssue();
				continue;
			}
		}

		if (buffer[MSG_TYPE] == (byte)MSG_TYPE_MEANING::CHECK_NEXT && _msg.empty()) {
			sendMsg(std::vector<byte>(), (byte)MSG_TYPE_MEANING::CHECK_NEXT);
			std::unique_lock<std::mutex> msgReceivedLck(_msgReceivedMtx);
			_msgReceived = true;
			msgReceivedLck.unlock();
			_msgReceivedCV.notify_one();
		}
		else {
			_msg.insert(_msg.end(), buffer, buffer + MSG_LEN);

			if (Utilities::getBit(buffer[OTHER_FLAGS], (byte)FLAGS::LAST_FRAGMENT_BIT)) {//if this is the last fragment of the message
				if (_msg.size() == 5632) {
					int i = 0;
				}
				std::unique_lock<std::mutex> dataReceivedLck(_dataReceivedMtx);
				_dataReceived.push(_msg);
				dataReceivedLck.unlock();

				_msg = std::vector<byte>();
				_dataReceivedCV.notify_one();
			}
		}
	}
}

void ConnectionLeft::timeoutMsgThread()
{
	std::unique_lock<std::mutex> connectionBrokenLck(_connectionBrokenMtx, std::defer_lock), msgReceivedLck(_msgReceivedMtx, std::defer_lock);
	while (!_endClass) {
		msgReceivedLck.lock();
		if (!_msgReceivedCV.wait_for(msgReceivedLck, std::chrono::seconds(WAIT_FOR_CHECK_MSG), [this] {return (bool)_msgReceived; })) {//if the node is an exit node, wait 60 seconds, else wait 20 seconds
			std::cout << std::string("TIMEOUT ") + std::to_string((int)_nodeName) + "\n";
			unsigned int timeToWait = 0;
			if (_nodeName == NODE_NAMES::EXIT_NODE) {
				timeToWait = WAIT_IF_EXIT;
			}
			else if (_nodeName == NODE_NAMES::MID_NODE) {
				timeToWait = WAIT_IF_MID;
			}
			connectionBrokenLck.lock();
			if (_connectionBrokenCV.wait_for(connectionBrokenLck, std::chrono::seconds(timeToWait), [this] { return /*_connectionBroken || */(bool)_endClass; })) {
				connectionBrokenLck.unlock();
				shutdownClass(true, true, false);
				return;
			}
			else {
				connectionBrokenLck.unlock();
			}
		}
		_msgReceived = false;
		msgReceivedLck.unlock();
	}
}

bool ConnectionLeft::sendSingleMsg(std::vector<byte> toSend, byte msgType, NODE_NAMES origin, bool isLast, int16_t lengthLast)
{
	std::unique_lock<std::mutex> connectionBrokenLck(_connectionBrokenMtx, std::defer_lock);
	int iResult = 0;
	byte flags = Utilities::setBit(0, (byte)FLAGS::LAST_FRAGMENT_BIT, isLast);
	if (origin == NODE_NAMES::NO_NODE) {
		origin = _nodeName;
	}
	if (origin == NODE_NAMES::GUARD_NODE) {
		flags = Utilities::setBit(flags, (byte)FLAGS::IS_GUARD, true);
	}
	else if (origin == NODE_NAMES::MID_NODE) {
		flags = Utilities::setBit(flags, (byte)FLAGS::IS_MID, true);
	}

	toSend.insert(toSend.begin(), flags);
	if (lengthLast != 0) {
		toSend.insert(toSend.begin() + sizeof(flags), msgType);
		std::vector<byte> length = Utilities::IntegerTypeTobigEndianBytes<int16_t>(lengthLast);//adding len to first msg
		toSend.insert(toSend.begin() + HEADER_SIZE, length.begin(), length.end());
	}
	toSend.resize(MSG_LEN);
	if (toSend.size() != 512) {
		std::cout << "ERROR\n";
	}
	iResult = send(_connectSocket, (char*)toSend.data(), (int)toSend.size(), 0);
	while (iResult == SOCKET_ERROR) {
		unsigned int timeToWait = 0;
		if (_nodeName == NODE_NAMES::EXIT_NODE) {
			timeToWait = WAIT_IF_EXIT;
		}
		else if (_nodeName == NODE_NAMES::MID_NODE) {
			timeToWait = WAIT_IF_MID;
		}
		connectionBrokenLck.lock();
		if (_connectionBrokenCV.wait_for(connectionBrokenLck, std::chrono::seconds(timeToWait), [this] {return /*_connectionBroken || */ (bool)_endClass; })) {
			return false;//TODO making it always enter because switching connections is not implemented yet
		}
		connectionBrokenLck.unlock();
		unsigned short haveBeenSent = 0;
		do {
			haveBeenSent += send(_connectSocket, (char*)toSend.data(), (int)toSend.size(), 0);
		} while (haveBeenSent < MSG_LEN);
	}
	return true;
}

std::vector<byte> ConnectionLeft::getData(std::vector<byte> msg, NODE_NAMES target)
{
	if (msg.size() > 5000) {
		int i = 0;
	}
	int16_t length = Utilities::bigEndianBytesToIntegerType<int16_t>(std::vector<byte>(msg.begin() + HEADER_SIZE, msg.begin() + FIRST_HEADER_LEN)), remainder;
	int lengthData;
	std::vector<byte> toReturn;
	if (msg.size() != MSG_LEN) {
		lengthData = length - FOLLOWING_HEADER_LEN + MSG_LEN - FIRST_HEADER_LEN;
		toReturn.insert(toReturn.end(), msg.begin() + FIRST_HEADER_LEN, msg.begin() + MSG_LEN);
	}
	else {
		lengthData = length - FIRST_HEADER_LEN;
		remainder = CryptoPP::AES::BLOCKSIZE - (lengthData % CryptoPP::AES::BLOCKSIZE);
		if (remainder == CryptoPP::AES::BLOCKSIZE) {
			remainder = 0;
		}
		toReturn.insert(toReturn.end(), msg.begin() + FIRST_HEADER_LEN, msg.begin() + length + remainder);

		if (_key.size() != 0) {
			toReturn = decrypt(toReturn);
			if (target == _nodeName) {
				toReturn = std::vector<byte>(toReturn.begin(), toReturn.end() - remainder);
			}
		}
		std::cout << "HERE\n:";
		return toReturn;
	}

	size_t i = MSG_LEN;

	while (msg.size() > i + MSG_LEN) {
		toReturn.insert(toReturn.end(), msg.begin() + i + FOLLOWING_HEADER_LEN, msg.begin() + i + MSG_LEN);
		lengthData += MSG_LEN - FOLLOWING_HEADER_LEN;
		i += MSG_LEN;
	}

	remainder = CryptoPP::AES::BLOCKSIZE - (lengthData % CryptoPP::AES::BLOCKSIZE);
	if (remainder == CryptoPP::AES::BLOCKSIZE) {
		remainder = 0;
	}

	toReturn.insert(toReturn.end(), msg.begin() + i + FOLLOWING_HEADER_LEN, msg.begin() + i + length + remainder);

	if (_key.size() != 0) {
		toReturn = decrypt(toReturn);
		if (target == _nodeName) {
			toReturn = std::vector<byte>(toReturn.begin(), toReturn.end() - remainder);
		}
	}
	if (toReturn.size() == 2144 || toReturn.size() == 2128 || toReturn.size() == 2097) {
		int i = 0;
	}
	return toReturn;
}

void ConnectionLeft::shutdownConnection()
{
	shutdown(_connectSocket, SD_SEND);
	std::unique_lock<std::mutex> connectionEndedLck(_connectionEndedMtx);
	if (!_connectionEnded && !_endClass) {
		_connectionEndedCV.wait(connectionEndedLck, [this] {return (bool)_connectionEnded; });
	}
	connectionEndedLck.unlock();
	closesocket(_connectSocket);
}

void ConnectionLeft::shutdownClass(bool closeConnection, bool timeoutCalled, bool recvMsgCalled)
{
	std::unique_lock<std::mutex> finishShutdownLck(_finishShutdownMtx, std::defer_lock), endClassLck(_endClassMtx), connectionBrokenLck(_connectionBrokenMtx, std::defer_lock);
	if (!_endClass) {
		finishShutdownLck.lock();
		_endClass = true;
		endClassLck.unlock();

		_endClassCV.notify_all();
		_dataReceivedCV.notify_all();
		_connectionBrokenCV.notify_all();//notified checks for _endClass
		if (closeConnection) {
			shutdownConnection();
		}
		if (!recvMsgCalled) {
			if (!_recvMsgsThread->joinable()) {
				std::cout << "Can't join\n";
			}
			_recvMsgsThread->join();
			delete _recvMsgsThread;
			_recvMsgsThread = nullptr;
			std::cout << "RECV MSG IS NOW nullptr\n";
		}
		if (!timeoutCalled && _timeoutMsgThread) {
			std::unique_lock<std::mutex> msgReceivedLck(_msgReceivedMtx);
			_msgReceived = true;
			msgReceivedLck.unlock();
			_msgReceivedCV.notify_one();
			_timeoutMsgThread->join();
			std::cout << "HERE\n";
			delete _timeoutMsgThread;
			_timeoutMsgThread = nullptr;
		}
	}
}

void ConnectionLeft::newSocket(SOCKET s)
{
	std::unique_lock<std::mutex> connectionBrokenLck(_connectionBrokenMtx, std::defer_lock);
	shutdownConnection();
	_connectSocket = s;
	_readEvent = WSACreateEvent();
	WSAEventSelect(_connectSocket, _readEvent, FD_READ | FD_CLOSE);//If socket closes, a recv will be attempted, will fail, and that will raise an error.
	connectionBrokenLck.lock();
	_connectionBroken = true;
	connectionBrokenLck.unlock();
	_connectionBrokenCV.notify_all();

	std::unique_lock<std::mutex> msgReceivedLck(_msgReceivedMtx);
	_msgReceived = true;
	msgReceivedLck.unlock();
	_msgReceivedCV.notify_all();
}

SOCKET ConnectionLeft::getSocket()
{
	return _connectSocket;
}

void ConnectionLeft::setNodeName(NODE_NAMES nodeName)
{
	_nodeName = nodeName;
	_timeoutMsgThread = new std::thread(&ConnectionLeft::timeoutMsgThread, this);
}

NODE_NAMES ConnectionLeft::getNodeName()
{
	return _nodeName;
}

ConnectionLeft::ConnectionLeft(SOCKET connectSocket)
{
	_connectSocket = connectSocket;
	_readEvent = WSACreateEvent();
	WSAEventSelect(_connectSocket, _readEvent, FD_READ | FD_CLOSE);//If socket closes, a recv will be attempted, will fail, and that will raise an error.
	_recvMsgsThread = new std::thread(&ConnectionLeft::recvMsgsThread, this);
	if (!keyExchange()) {
		throw KeyExchangeError();
	}
}

ConnectionLeft::~ConnectionLeft()
{
	shutdownClass();
	std::unique_lock<std::mutex> finishShutdownLck(_finishShutdownMtx);//making sure shutdownClass finished shutting down
	finishShutdownLck.unlock();
	if (_recvMsgsThread != nullptr) {
		_recvMsgsThread->join();
		std::cout << "SUCCESSFULLY JOINED\n";
		delete _recvMsgsThread;
	}
	if (_timeoutMsgThread != nullptr) {
		_timeoutMsgThread->join();
		delete _timeoutMsgThread;
	}
}


void ConnectionLeft::operator=(ConnectionLeft&& other) noexcept
{
	other.shutdownClass(false);
	_dataReceived = other._dataReceived;
	_connectSocket = (SOCKET)other._connectSocket;
	_nodeName = other._nodeName;
	_recvMsgsThread = new std::thread(&ConnectionLeft::recvMsgsThread, this);
	_timeoutMsgThread = new std::thread(&ConnectionLeft::timeoutMsgThread, this);
}

bool ConnectionLeft::operator==(const SOCKET& otherSocket) const
{
	return otherSocket == _connectSocket;
}

bool ConnectionLeft::sendMsg(std::vector<byte> toSend, byte msgType, NODE_NAMES origin, int16_t length)
{
	if (_endClass) {
		return false;
	}
	uint16_t newLen;
	if (length == 0) {
		newLen = toSend.size();
	}
	else {
		newLen = length;
	}
	if (_key.size() != 0) {//if key has been initialized
		toSend = encrypt(toSend);
		newLen += CryptoPP::AES::BLOCKSIZE;
	}
	
	std::unique_lock<std::mutex> sendLck(_sendMtx);
	if (MSG_LEN - FIRST_HEADER_LEN >= toSend.size()) {
		if (length == 0) {
			newLen += FIRST_HEADER_LEN;
		}
		return sendSingleMsg(std::vector<byte>(toSend.begin(), toSend.begin() + toSend.size()), msgType, origin, true, newLen);//because this will be the only msg, can just return if it succeeded or not
	}
	int16_t lastFragSize, remainder = (toSend.size() - (MSG_LEN - FIRST_HEADER_LEN)) % (MSG_LEN - FOLLOWING_HEADER_LEN);
	remainder %= CryptoPP::AES::BLOCKSIZE;

	if (length == 0) {
		lastFragSize = (uint16_t)(newLen - (MSG_LEN - FIRST_HEADER_LEN)) % (MSG_LEN - FOLLOWING_HEADER_LEN) + FOLLOWING_HEADER_LEN;
		if (lastFragSize == FOLLOWING_HEADER_LEN) {//if last message would've been empty except for flag
			lastFragSize = MSG_LEN;
		}
	}
	else {
		lastFragSize = newLen;
	}

	if (lastFragSize + remainder > MSG_LEN - FOLLOWING_HEADER_LEN) {
		lastFragSize -= MSG_LEN;
	}
	
	if (!sendSingleMsg(std::vector<byte>(toSend.begin(), toSend.begin() + MSG_LEN - FIRST_HEADER_LEN), msgType, origin, false, lastFragSize)) {//calculating length of the last msg
		return false;
	}

	size_t i = MSG_LEN - FIRST_HEADER_LEN;
	while (toSend.size() > i + MSG_LEN - FOLLOWING_HEADER_LEN) {
		if (!sendSingleMsg(std::vector<byte>(toSend.begin() + i, toSend.begin() + i + MSG_LEN - FOLLOWING_HEADER_LEN), msgType, origin)) {
			return false;
		}
		i += MSG_LEN - FOLLOWING_HEADER_LEN;
	}
	if (!sendSingleMsg(std::vector<byte>(toSend.begin() + i, toSend.end()), msgType, origin, true)) {
		return false;
	}
	return true;
}

bool ConnectionLeft::recvOne(std::vector<byte>& recvMsg, std::array<byte, 2>& flags, int16_t& length, std::chrono::milliseconds waitTime)
{
	recvMsg.clear();
	std::unique_lock<std::mutex> dataReceivedLck(_dataReceivedMtx);
	if (!_dataReceived.empty()) {
		recvMsg = _dataReceived.front();
		_dataReceived.pop();
		flags[0] = Utilities::setBit(recvMsg[OTHER_FLAGS], (byte)FLAGS::LAST_FRAGMENT_BIT, false);
		flags[1] = recvMsg[MSG_TYPE];
		if (flags[1] == 6) {
			int i = 0;
		}
		if (length == 0) {
			length = Utilities::bigEndianBytesToIntegerType<int16_t>(std::vector<byte>(recvMsg.begin() + FIRST_HEADER_LEN - HEADER_SIZE, recvMsg.begin() + FIRST_HEADER_LEN));
			if (_key.size() != 0 && flags[MSG_TYPE] != (byte)MSG_TYPE_MEANING::PATH_CLOSURE_MSG) {
				if (length < 0) {
					length = length - CryptoPP::AES::BLOCKSIZE + MSG_LEN;
				}
				else {
					length -= CryptoPP::AES::BLOCKSIZE;
				}
			}
		}
		NODE_NAMES target;
		if (Utilities::getBit(flags[0], 1)) {
			target = NODE_NAMES::GUARD_NODE;
		}
		else if (Utilities::getBit(flags[0], 2)) {
			target = NODE_NAMES::MID_NODE;
		}
		else {
			target = NODE_NAMES::EXIT_NODE;
		}
		recvMsg = getData(recvMsg, target);
		return true;
	}

	if (_endClass == true) {
		return false;
	}
	while (!_endClass && _dataReceived.empty()) {
		if (waitTime == std::chrono::milliseconds(0)) {
			_dataReceivedCV.wait(dataReceivedLck, [this] {return _endClass || _dataReceived.size() > 0; });
		}
		else {
			if (_dataReceivedCV.wait_for(dataReceivedLck, waitTime, [this] {return _endClass || _dataReceived.size() > 0; })) {
				return true;
			}
		}
	}
	if (_endClass == true) {
		return false;
	}
	recvMsg = _dataReceived.front();
	_dataReceived.pop();
	flags[0] = Utilities::setBit(recvMsg[OTHER_FLAGS], (byte)FLAGS::LAST_FRAGMENT_BIT, false);
	flags[1] = recvMsg[MSG_TYPE];
	if (flags[1] == 6) {
		int i = 0;
	}
	if (length == 0) {
		length = Utilities::bigEndianBytesToIntegerType<int16_t>(std::vector<byte>(recvMsg.begin() + FIRST_HEADER_LEN - HEADER_SIZE, recvMsg.begin() + FIRST_HEADER_LEN));
		if (_key.size() != 0 && flags[MSG_TYPE] != (byte)MSG_TYPE_MEANING::PATH_CLOSURE_MSG) {
			if (length < 0) {
				length = length - CryptoPP::AES::BLOCKSIZE + MSG_LEN;
			}
			else {
				length -= CryptoPP::AES::BLOCKSIZE;
			}
		}
	}
	NODE_NAMES target;
	if (Utilities::getBit(flags[0], 1)) {
		target = NODE_NAMES::GUARD_NODE;
	}
	else if (Utilities::getBit(flags[0], 2)) {
		target = NODE_NAMES::MID_NODE;
	}
	else {
		target = NODE_NAMES::EXIT_NODE;
	}
	if (length == 489 && recvMsg.size() == 2560) {
		int i = 0;
	}
	recvMsg = getData(recvMsg, target);
	return true;
}

bool ConnectionLeft::recvOne(std::vector<byte>& recvMsg, std::array<byte, 2>& flags, std::chrono::milliseconds waitTime)
{
	int16_t lengthTemp = 0;
	return recvOne(recvMsg, flags, lengthTemp, waitTime);
}
