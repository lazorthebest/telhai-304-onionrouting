#pragma once
//OC
#include <vector>
#include "..\cryptopp\dll.h"
#include "..\cryptopp\filters.h"
#include "..\cryptopp\aes.h"
#include "..\cryptopp\modes.h"
#include "..\cryptopp\integer.h"
#include "..\cryptopp\osrng.h"
#include "..\cryptopp\sha.h"
#define DH_KEY_LEN 256
#define PRIVATE_EXPONENT_LEN 32
#define BITS_IN_BYTE 8
class EncryptionUtils
{
	static CryptoPP::Integer getMod();
public:
	//converts a DH_KEY_LEN byte array to a 32 byte sized key.
	static std::vector<CryptoPP::byte> generateKey(CryptoPP::byte toHash[DH_KEY_LEN]);

	//generates a random byte vector at a numBytes len.
	static std::vector<CryptoPP::byte>generateRand(size_t numBytes);

	static std::vector<CryptoPP::byte> encrypt(std::vector<CryptoPP::byte> msg,
		CryptoPP::byte key[CryptoPP::AES::MAX_KEYLENGTH],
		CryptoPP::byte iv[CryptoPP::AES::BLOCKSIZE]);

	static std::vector<CryptoPP::byte> decrypt(std::vector<CryptoPP::byte> cipher,
		CryptoPP::byte key[CryptoPP::AES::MAX_KEYLENGTH],
		CryptoPP::byte iv[CryptoPP::AES::BLOCKSIZE]);

	//will modulate. mod = getMod()
	static std::vector<CryptoPP::byte> power(CryptoPP::byte base[DH_KEY_LEN],
		CryptoPP::byte exponent[PRIVATE_EXPONENT_LEN]);

	//2 is the base.
	//will modulate. mod = getMod()
	static std::vector<CryptoPP::byte> power2(CryptoPP::byte exponent[PRIVATE_EXPONENT_LEN]);

	static size_t getIvLen() { return CryptoPP::AES::BLOCKSIZE; };
};

