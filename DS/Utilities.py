
#loc starts from 0
def get_byte(value, loc):
    return value & (1 << loc) != 0


def set_byte(arr):
    byte = 0
    for i in range(len(arr)):
        if arr[i] is True:
            byte += 2 ** i
    return byte
