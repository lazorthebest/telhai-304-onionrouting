import asyncio
import secrets
import Communicator
import Debug
import Globals
from Crypto.Cipher import AES
from Crypto.Util .Padding import pad
from hashlib import sha256

class OnionRouter:
	def __init__(self, socket, key):
		self.socket = socket
		self.key = key

class MsgTypes:
	GIVE_NODE = 0
	GET_NEW_NODE = 1
	REPORT_NODE = 2
	GET_GUARD_NODE = 3
	NODE_ALIVE = 4


TOKEN_LEN_BITS = 128
TOKEN_LEN_BYTES = 16


class ConnHandler:

	def __init__(self, new_conns, new_conns_cv, new_conns_lck, nodes,):
		self.new_conns = new_conns
		self.new_conns_cv = new_conns_cv
		self.new_conns_lck = new_conns_lck
		self.nodes = nodes

	def start(self):
		Debug.mpl.info("start")
		loop = asyncio.new_event_loop()
		loop.create_task(self.start_coroutine(loop))
		loop.run_forever()

	async def start_coroutine(self, loop):
		while True:
			await self.new_conns_cv.coro_acquire()
			if self.new_conns.empty():# todo is queue enough without locks?
				Debug.mpl.info("start cv waits")
				await self.new_conns_cv.coro_wait() # todo remove except?
				Debug.mpl.info("start cv finished wait")
			new_conn = self.new_conns.get()
			if type(new_conn) is bool:  # means we need to terminate
				self.new_conns.put(new_conn)
				self.new_conns_lck.release()
				loop.stop()
				break
			self.new_conns_lck.release()
			loop.create_task(self.handle_new_conn(new_conn, loop))

	async def handle_new_conn(self, new_conn, loop):
		Debug.mpl.info("handle cv waits")
		try:
			msg = await Communicator.recv(new_conn.socket, loop)
		except Exception: #conn closure
			return
		Debug.mpl.info("handle cv finished wait")
		if not Globals.DATA_STARTS > len(msg):
			private_key = secrets.randbits(Globals.LEN_OF_KEY_BITS)
			my_public = pow(Globals.g, private_key, Globals.p)
			my_public = int.to_bytes(my_public, int(Globals.LEN_OF_KEY_EXCHANGE_KEY_BITS / 8), "big")
			remote_pub = int.from_bytes(msg[:int(Globals.LEN_OF_KEY_EXCHANGE_KEY_BITS/8)], "big")
			key_exchange_key = int.to_bytes(pow(remote_pub, private_key, Globals.p),int(Globals.LEN_OF_KEY_EXCHANGE_KEY_BITS/8),"big")
			encryption_key = sha256(key_exchange_key).digest()
			print("ENCRYPTION KEY:")
			print(list(encryption_key))
			if msg[Globals.DATA_STARTS] == MsgTypes.NODE_ALIVE:
				self.nodes[new_conn.ip] = OnionRouter(new_conn.socket, encryption_key)
				try:
					await Communicator.send(my_public, new_conn.socket, loop)
				except Exception as exc:
					print(exc)
			elif msg[Globals.DATA_STARTS] == MsgTypes.GET_NEW_NODE:
				await self.get_new_node(new_conn,encryption_key, my_public, loop)
			elif msg[Globals.DATA_STARTS] == MsgTypes.GIVE_NODE:
				await self.get_nodes(new_conn, encryption_key, my_public, loop)
		else:#msg[Globals.DATA_STARTS] == MsgTypes.GET_GUARD_NODE:#ip is not sensitive so not encrytped.
			ip = secrets.choice(list(self.nodes.keys()))
			try:
				await Communicator.send(ip, new_conn.socket, loop)
			except Exception as exc:
				print(exc)
		# node reporting unimplemented.

	async def get_new_node(self, new_conn, encryption_key, my_public, loop):
		token, ip, = await self.send_token_to_OR(loop)
		cipher = AES.new(encryption_key, AES.MODE_CBC)
		msg = cipher.encrypt(pad(ip+token,AES.block_size))
		msg =my_public + cipher.iv + msg
		try:
			await Communicator.send(msg, new_conn.socket, loop)
		except Exception: pass

	async def get_nodes(self, new_conn,encryption_key, my_public, loop):
		token1, ip1 = await self.send_token_to_OR(loop)
		token2, ip2 = await self.send_token_to_OR(loop)
		cipher = AES.new(encryption_key, AES.MODE_CBC)
		msg = cipher.encrypt(pad(ip1+token1 + ip2 + token2,AES.block_size))
		msg = my_public + cipher.iv + msg
		print("GET NODES CLIENT IV: ")
		print(list(cipher.iv))
		try:
			await Communicator.send(msg, new_conn.socket, loop)
		except Exception: pass

	async def send_token_to_OR(self, loop):
		while True:
			ip, onion_router = secrets.choice(list(self.nodes.items()))
			token = int.to_bytes(secrets.randbits(TOKEN_LEN_BITS), TOKEN_LEN_BYTES, "big")
			cipher = AES.new(onion_router.key, AES.MODE_CBC)
			msg = cipher.iv + cipher.encrypt(token)
			print("OR IV: ")
			print(list(cipher.iv))
			print("OR TOKNE:")
			print(list(token))
			try:
				await Communicator.send(msg, onion_router.socket, loop)
				break
			except Exception:
				try:
					del self.nodes[ip]
				except Exception: pass
				continue
		return token, ip
