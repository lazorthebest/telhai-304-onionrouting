LEN_SIZE = 2

#RAISES Exception and socket excpetions
async def recv(socket, loop,):
    buff = await loop.sock_recv(socket, LEN_SIZE)
    if len(buff) == 0:
        raise Exception
    size = int.from_bytes(buff, "big")

    to_return = b""
    buff = b""
    while len(buff) < size:
        buff = await loop.sock_recv(socket, 1024)
        if len(buff) == 0:
            raise Exception
        to_return += buff
    return to_return


#RAISES socket excpetions
async def send(msg, socket, loop):
    size = int.to_bytes(len(msg), LEN_SIZE, "big")
    await loop.sock_sendall(socket, size)
    await loop.sock_sendall(socket, msg)
