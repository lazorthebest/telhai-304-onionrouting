#include "PathGenerator.h"

PathGenerator& PathGenerator::instance(std::vector<byteVec> DSIPs)
{
	static PathGenerator pg(DSIPs);
	return pg;
}

PathGenerator::PathGenerator(std::vector<byteVec> DSIPs)
{
	std::vector<byte> DSIP;
	unsigned int i = 0;
	if (DSIPs.size() == 0) {
		DSIP = DBManager::instance().getDSIP();
	}
	else {
		DSIP = DSIPs[i % DSIPs.size()];
		i++;
	}
	_cm.setCommunicationDetails(DSIP);
}

std::pair<byteVec, byteVec> PathGenerator::getNewNode()
{
	bool success = false;
	byteVec response;
	byteVec decryptedResponse;
	byteVec privateKey;
	byteVec msg;
	byteVec key;
	byteVec dhKey;
	byteVec publicKey;
	publicKey.resize(DH_KEY_LEN);
	std::lock_guard<std::mutex> lck(_cmMtx);
	while (!success) {
		try {
			connectDS();
			privateKey = EncryptionUtils::generateRand(PRIVATE_EXPONENT_LEN);
			msg = EncryptionUtils::power2(privateKey.data());//public key
			msg.push_back((byte)DS_MSGS::GET_NEW_NODE);
			_cm.sendToServer(msg);
			response = _cm.receiveFromServer(SEC_IN_MILLI * SECS_TO_WAIT);
			_cm.closeCommunication();

			publicKey.assign(response.begin(), response.begin() + DH_KEY_LEN);
			dhKey = EncryptionUtils::power(publicKey.data(), privateKey.data());
			key = EncryptionUtils::generateKey(dhKey.data());
			decryptedResponse = EncryptionUtils::decrypt(
				byteVec(response.begin() + DH_KEY_LEN + EncryptionUtils::getIvLen(), response.end()),
				key.data(),
				response.data() + DH_KEY_LEN);
			success = true;
		}
		catch (TimeOut) {
			_cm.closeCommunication();
		}
		catch (CommunicationException) {
			_cm.closeCommunication();
		}
	}
	return std::pair<byteVec, byteVec>(byteVec(response.begin(), response.begin() + IP_LEN), byteVec(response.begin() + IP_LEN, response.end()));
}

void PathGenerator::establishConnWithGuard(PathManager* pm,byteVec ip, NODE_NAMES overrideNodeFailure)
{
	bool success = false;
	byteVec response;
	byteVec toSend;

	if (ip.size() == 0) {
		ip = DBManager::instance().getGuardNode().second;
	}
	while (!success) {
		try {
			performORKeyExchange(pm, NODE_NAMES::GUARD_NODE, overrideNodeFailure);
			pm->sendMsgDirectly(toSend, MSG_TYPES::PATH_GENERATION,NODE_NAMES::GUARD_NODE,overrideNodeFailure);
			response = pm->recvPathGeneration(NO_TIMEOUT, overrideNodeFailure);
			break;
		}
		catch (NodeCrashException) {
		}
		//the threads responsible for reconnect will handle the disconnect.
		pm->waitForPathToBeOk(false);
	}
	pm->setNodeDetails(NODE_NAMES::GUARD_NODE, ip);
}


bool PathGenerator::exchangeTokenWithNode(std::pair<byteVec, byteVec>& ipAndToken, NODE_NAMES dst, PathManager* pm, NODE_NAMES overrideNodeFailure)
{
	byteVec toSend;
	byteVec response;
	while (true) {
		toSend = std::vector<byte>(ipAndToken.second.begin(), ipAndToken.second.begin() + TOKEN_LEN / 2);
		try {
			pm->sendMsgDirectly(toSend, MSG_TYPES::PATH_GENERATION,dst, overrideNodeFailure);
			response = pm->recvPathGeneration(NO_TIMEOUT, overrideNodeFailure);
			if (response[0] == TRUE && std::vector<byte>(response.begin() + sizeof(byte), response.begin() + sizeof(byte) + TOKEN_LEN / 2)
				== std::vector<byte>(ipAndToken.second.begin() + TOKEN_LEN / 2, ipAndToken.second.begin() + TOKEN_LEN))
			{
				return true;
			}
			else {
				return false;
			}
		}
		catch (NodeCrashException) {
			pm->waitForPathToBeOk(false);
			continue;
		}
		ipAndToken = getNewNode();
	}
}

void PathGenerator::connectToNode(std::pair<byteVec, byteVec>& ipAndToken, NODE_NAMES dst, PathManager* pm, NODE_NAMES overrideNodeFailure)
{
	byteVec response;
	while (true) {
		try {
			pm->sendMsgDirectly(ipAndToken.first, MSG_TYPES::PATH_GENERATION, dst, overrideNodeFailure );
			response = pm->recvPathGeneration(NO_TIMEOUT, overrideNodeFailure);
			if (response[0] == TRUE) {
				break;
			}
		}
		catch (NodeCrashException) {
			pm->waitForPathToBeOk(false);
			continue;
		}
		ipAndToken = getNewNode();
	}
}

 

void PathGenerator::establishConnWithNonGuard(NODE_NAMES dst, PathManager* pm, std::pair<byteVec, byteVec> ipAndToken, NODE_NAMES overrideNodeFailure)
{
	bool success = false;
	byteVec response;
	byteVec toSend;
	if (ipAndToken.first.size() == 0) {
		ipAndToken = getNewNode();
	}
	while (true) {
		connectToNode(ipAndToken, (NODE_NAMES)((byte)dst - 1), pm, overrideNodeFailure);//connecting to the mid/exit node.
		performORKeyExchange(pm, dst, overrideNodeFailure);
		//exchanging tokens with the node.
		if (!exchangeTokenWithNode(ipAndToken, dst, pm, overrideNodeFailure)) {
			ipAndToken = getNewNode();
		}
		else {
			break;
		}
	}
	pm->setNodeDetails(dst, ipAndToken.first, ipAndToken.second);
}


void PathGenerator::connectDS(std::vector<byteVec> DSIPs)
{
	bool success = false;
	unsigned int i = 0;
	byteVec DSIP;
	if (DSIPs.size() == 0) {
		DSIP = DBManager::instance().getDSIP();
	}
	else {
		DSIP = DSIPs[i];
		i++;
	}
	while (!success) {
		try {
			_cm.setCommunicationDetails(DSIP);
			_cm.connectToServer();			
			success = true;
		}
		catch (CommunicationException) {
			if (Utilities::isIPV4InternetConnectionOk()) {
				if (DSIPs.size() == 0) {
					DSIP = DBManager::instance().getDSIP();
				}
				else {
					DSIP = DSIPs[i%DSIPs.size()];
					i++;
				}
			}
		}
	}
}

bool PathGenerator::reportNode(byteVec nodeIp)
{
	bool reportedSuccessfully = false;
	if (Utilities::isIPV4InternetConnectionOk()) {
		byteVec toSend;
		byteVec temp;
		toSend.push_back((byte)DS_MSGS::REPORT_NODE);
		toSend.insert(toSend.end(), nodeIp.begin(), nodeIp.end());
		std::lock_guard<std::mutex> lck(_cmMtx);
		while (!reportedSuccessfully) {
			try { 
				connectDS();
				_cm.sendToServer(toSend);
				temp = _cm.receiveFromServer(SEC_IN_MILLI*SECS_TO_WAIT);//receiving msg as confirmation
				_cm.closeCommunication();
				reportedSuccessfully = true;
			}
			catch (CommunicationException) {//changing the DS, if there still is no internet issue
				_cm.closeCommunication();
				if (Utilities::isIPV4InternetConnectionOk()) {
					continue;
				}
				else {
					return false;//perhaps internt issues caused the node to not respond.
				}
			}
			catch (TimeOut) {
				_cm.closeCommunication();
				if (Utilities::isIPV4InternetConnectionOk()) {
					continue;
				}
				else {
					return false;//perhaps internt issues caused the node to not respond.
				}
			}
		}
		return true;
	}
	else {
		return false;
	}
}

void PathGenerator::reconnectGuardNode(PathManager* pm)
{
	byteVec ip;
	byteVec received;
	byteVec tempVec;
	bool success = false;
	try{
		std::pair<byteVec, byteVec> ipAndToken = pm->getNodeDetails(NODE_NAMES::MID_NODE);
		//if the new guard will fail it will be auto replaced, thus
		//this will prevent the raplacing function from connecting to the mid inself.
		pm->setNodeDetails(NODE_NAMES::MID_NODE, {});
		while (true) {
			//sending
			ip = pm->connectToNewGuardNode();
			establishConnWithGuard(pm, ip, NODE_NAMES::GUARD_NODE);
			//building the msg
			establishConnWithNonGuard(NODE_NAMES::MID_NODE, pm, ipAndToken, NODE_NAMES::GUARD_NODE);
		}
	}
	catch (UninitiatedValue) {//means we didn't connect to a mid, so only re connect to the guard.
		ip = pm->connectToNewGuardNode();
		establishConnWithGuard(pm, ip, NODE_NAMES::GUARD_NODE);
	}

}
void PathGenerator::reconnectNonGuardNode(NODE_NAMES node, PathManager* pm)
{
	byteVec msg;
	byteVec received;
	std::pair<byteVec, byteVec> ipAndToken;
	int success = false;
	if (node == NODE_NAMES::MID_NODE) {
		try {
			pm->getNodeDetails(NODE_NAMES::EXIT_NODE);
		}
		catch (UninitiatedValue) {//means no exit node exists, so we just need to reconnect to the mid.
			establishConnWithNonGuard(node, pm, getNewNode(), node);
			return;
		}
		ipAndToken = pm->getNodeDetails(NODE_NAMES::EXIT_NODE);
		pm->setNodeDetails(NODE_NAMES::EXIT_NODE, {});//if say the mid crashes while replacing the exit, this will prevent additional replacment functions from trying to connect to the exit.

		//connecting to a new mid;
		establishConnWithNonGuard(NODE_NAMES::MID_NODE, pm, getNewNode(), node);
		//connecting to the existing exit(if it fails will connect to another)
		establishConnWithNonGuard(node, pm, getNewNode(), node);
	}
	else {
		establishConnWithNonGuard(node, pm, getNewNode(), node);
		//ConnectionFactory::instance().updatePm();
	}
}

void PathGenerator::performORKeyExchange(PathManager* pm, NODE_NAMES dst, NODE_NAMES overrideNodeFailure)
{
	byteVec decryptedResponse;
	byteVec privateKey;
	byteVec dhKey;
	byteVec response;
	byteVec publicKey;
	privateKey = EncryptionUtils::generateRand(DH_KEY_LEN);
	pm->sendMsgDirectly(EncryptionUtils::power2(privateKey.data()) ,MSG_TYPES::KEY_EXCHANGE, dst, overrideNodeFailure);//sending the public key
	response = pm->recvPathGeneration(NO_TIMEOUT);

	publicKey.assign(response.begin(), response.begin() + DH_KEY_LEN);
	dhKey = EncryptionUtils::power(publicKey.data(), privateKey.data());
	pm->setKey(EncryptionUtils::generateKey(dhKey.data()), dst);
}

PathManager* PathGenerator::generatePath()
{
	bool success = false;
	PathManager* pm = nullptr;
	byteVec decryptedResponse;
	byteVec privateKey;
	byteVec msg;
	byteVec key;
	byteVec dhKey;
	byteVec publicKey;
	std::queue<byteVec> ips;
	std::queue<byteVec> tokens;
	byteVec response; 
	std::pair<byteVec,byteVec> ipAndToken;
	std::unique_lock<std::mutex> lck(_cmMtx,std::defer_lock);//getting the tokens and IPS from the server.
	lck.lock();
	while (!success) {
		try {
			connectDS();
			privateKey = EncryptionUtils::generateRand(DH_KEY_LEN);
			msg = EncryptionUtils::power2(privateKey.data());//public key
			msg.push_back((byte)DS_MSGS::GIVE_NODES);
			_cm.sendToServer(msg);
			response = _cm.receiveFromServer(SEC_IN_MILLI * SECS_TO_WAIT);
			_cm.closeCommunication();

			publicKey.assign(response.begin(), response.begin() + DH_KEY_LEN);
			dhKey = EncryptionUtils::power(publicKey.data(), privateKey.data());
			key = EncryptionUtils::generateKey(dhKey.data());
			decryptedResponse = EncryptionUtils::decrypt(
				byteVec(response.begin() + DH_KEY_LEN + EncryptionUtils::getIvLen(), response.end()),
				key.data(),
				response.data() + DH_KEY_LEN);
			success = true;
		}
		catch(TimeOut){
			_cm.closeCommunication();
			continue;
		}
		catch(CommunicationException){
			_cm.closeCommunication();
			continue;
		}
	}	
	lck.unlock();
	success = false;
	//extracting ips and tokens from the data.	
	for (int i = 0; i < (IP_LEN + TOKEN_LEN) * NODES_RECV_FROM_DS; i += IP_LEN + TOKEN_LEN) {
		ips.push(std::vector<byte>(decryptedResponse.begin() + i, decryptedResponse.begin() + i + IP_LEN));
		tokens.push(std::vector<byte>(decryptedResponse.begin() + i + IP_LEN, decryptedResponse.begin() +i + IP_LEN + TOKEN_LEN));
	}
	ipAndToken.first = DBManager::instance().getGuardNode().second;
	while (!success) {
		try {
			pm = new PathManager(ipAndToken.first);
			success = true;
		}
		catch (CommunicationException) {
			if (Utilities::isIPV4InternetConnectionOk()) {
				reportNode(ips.front());
				ipAndToken = getNewNode();
			}
		}
	}
	establishConnWithGuard(pm, ipAndToken.first);
	
	establishConnWithNonGuard(NODE_NAMES::MID_NODE, pm, std::pair<byteVec, byteVec>(ips.front(), tokens.front()));
	ips.pop();
	tokens.pop();

	establishConnWithNonGuard(NODE_NAMES::EXIT_NODE, pm, std::pair<byteVec, byteVec>(ips.front(), tokens.front()));
	ips.pop();
	tokens.pop();

	std::lock_guard<std::mutex> primaryPathlck(_primaryPathMtx);
	if (_primaryPath != nullptr) {
		_primaryPath->setIsPrimary(false);
	}
	_primaryPath = pm;
	return pm;
}

byteVec PathGenerator::getGuardFromDS(std::vector<byteVec> guardIPs,std::vector<byteVec> DSIPs)
{
	std::unique_lock<std::mutex> lck(_cmMtx, std::defer_lock);
	byteVec response, toSend = { (byte)DS_MSGS::GET_GUARD_NODE };
	for (byteVec guardIP : guardIPs) {
		toSend.insert(toSend.end(), guardIP.begin(), guardIP.end());
	}
	lck.lock();
	while (true) {
		try {
			connectDS(DSIPs);
			_cm.sendToServer(toSend);
			response = _cm.receiveFromServer(SEC_IN_MILLI*SECS_TO_WAIT);
			_cm.closeCommunication();
			if (response[0] == FAIL) {
				throw NoGuardsAvailException();
			}
			else {
				break;
			}
		}	
		catch (TimeOut) {
			_cm.closeCommunication();
			continue;
		}
		catch (CommunicationException) {
			_cm.closeCommunication();
			continue;
		}
	}
	lck.unlock();
	return byteVec(response.begin(), response.end());
}

