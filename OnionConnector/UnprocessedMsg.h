#pragma once
#include <vector>
#include "exceptions.cpp"
#include "InterThreadMsg.h"
#include "Communicator.h"
enum class NODE_NAMES;
enum class MSG_TYPES;

typedef unsigned char byte;
class UnprocessedMsg :
	public InterThreadMsg
{
	MSG_TYPES _flag;
	NODE_NAMES _origin;
	std::vector<byte> _msg;
	bool _isInit;
	byte _paddingLen;
public:
	UnprocessedMsg() {
		_isInit = false;
	};

	UnprocessedMsg(const UnprocessedMsg& unprMsg) {
		_isInit = true;
		_msg = unprMsg.getMsg();
		_origin = unprMsg.getOrigin();
		_flag = unprMsg.getFlag();
		_paddingLen = unprMsg.getPaddingLen();
	}
	UnprocessedMsg(NODE_NAMES origin, MSG_TYPES flag, byte paddingLen, std::vector<byte> msg) {
		_isInit = true;
		_msg = std::vector<byte>(msg.begin(), msg.end());
		_origin = origin;
		_flag = flag;
		_paddingLen = paddingLen;
	}

	std::vector<byte> getMsg() const{
		if (!_isInit) {
			throw UninitiatedValue();
		}
		return _msg;
	}
	MSG_TYPES getFlag()const {
		if (!_isInit) {
			throw UninitiatedValue();
		}
		return _flag;
	}

	NODE_NAMES getOrigin()const {
		if (!_isInit) {
			throw UninitiatedValue();
		}
		return _origin;
	}	
	
	byte  getPaddingLen()const {
		if (!_isInit) {
			throw UninitiatedValue();
		}
		return _paddingLen;
	}
	virtual ~UnprocessedMsg() {};
	virtual std::string getType() { return "UnprocessedMsg"; };
};
