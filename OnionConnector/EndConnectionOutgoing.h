#pragma once
#include "Utilities.h"
#include "InterThreadMsg.h"
#include <vector>

typedef unsigned char byte;

class EndConnectionOutgoing:public InterThreadMsg
{
	std::vector<byte> _connDetails;
public:
	EndConnectionOutgoing(std::vector<byte> id);
	std::vector<byte> getConnDetails() {
		return  _connDetails;
	}
	virtual std::string getType() {
		return "EndConnectionOutgoing";
	}
	virtual ~EndConnectionOutgoing() {};
};

