#include "PathGenerator.h"
#include "PathManager.h"


void PathManager::sendConn(InterThreadMsg * msg)
{
	byteVec encryptedMsg;
	PMConnComm* commStruct = nullptr;
	byte paddingLen = 0;
	std::unique_lock<std::mutex> cmSendLck(_cmSendMtx, std::defer_lock);
	std::unique_lock<std::mutex> connsLck(_connsMtx, std::defer_lock);
	bool toSuicide = false;
	if (msg->getType() == "ConnTerminated") {//conn has been successfully closed.
		connsLck.lock();
		_conns.erase( ((ConnTerminated*)msg)->getId());
		if (_conns.empty()) {
			connsLck.unlock();
			if (!_isPrimary) {
				toSuicide = true;
			}
		}
		else {
			connsLck.unlock();
		}
	}
	else {
		while (true) {
			waitForPathToBeOk();//if it is already ok, will not wait.
			try {
				if (msg->getType() == "ToSendMsg") {
					byteVec encryptedMsg = encrypt(((ToSendMsg*)msg)->getMsg(), NODE_NAMES::EXIT_NODE);
					byte paddingLen = encryptedMsg.size() - ((ToSendMsg*)msg)->getMsg().size();
					paddingLen -= CryptoPP::AES::BLOCKSIZE* NUM_OF_NODES;
					cmSendLck.lock();
					_cm.sendToServer(encryptedMsg, paddingLen);
					cmSendLck.unlock();
				}
				else {//EndConnectionMsg, send request to close connection.
					byteVec encryptedMsg = encrypt(((EndConnectionOutgoing*)msg)->getConnDetails(), NODE_NAMES::EXIT_NODE);
					byte paddingLen = encryptedMsg.size() - ((EndConnectionOutgoing*)msg)->getConnDetails().size();
					paddingLen -= CryptoPP::AES::BLOCKSIZE* NUM_OF_NODES;
					cmSendLck.lock();
					_cm.sendToServer(encryptedMsg, paddingLen, MSG_TYPES::CONN_CLOSURE);
					cmSendLck.unlock();
				}
				break;
			}
			catch (CommunicationException exc) {//timeout exception.
				std::unique_lock<std::mutex> lck(_nodeStatusesMtx);//ensuring the path won't be fixed before starting to wait, leading to a possibly infinitr wait.
				cmSendLck.unlock();
				waitForPathToBeOk(lck, true);
			}
		}
	}
	delete msg;
	if (toSuicide) {
		delete this;
	}
}

std::vector<byte> PathManager::encrypt(std::vector<byte> msg, NODE_NAMES target)
{
	if (target == NODE_NAMES::MID_NODE) {
		int i = 0;
	}
	if (msg.size() == 0) {
		return std::vector<byte>();
	}
	std::vector<byte> toReturn = msg;
	for (int i = (byte)target; i >= 0; i--) {
		std::vector<byte> iv = EncryptionUtils::generateRand(CryptoPP::AES::BLOCKSIZE);//generating the initial vector
		toReturn = EncryptionUtils::encrypt(toReturn, _keys[i].data(), iv.data());
		toReturn.insert(toReturn.begin(), iv.begin(), iv.end());
	}
	return toReturn;
}

std::vector<byte> PathManager::decrypt(std::vector<byte> cipherText, NODE_NAMES origin)
{
	std::vector<byte> toReturn = cipherText;
	if (cipherText.size() == 0) {
		return std::vector<byte>();
	}
	for (int i = 0; i <= (byte)origin; i++) {
		std::vector<byte> iv = std::vector<byte>(toReturn.begin(), toReturn.begin() + CryptoPP::AES::BLOCKSIZE);//getting the initial vector
		toReturn = EncryptionUtils::decrypt(std::vector<byte>(toReturn.begin() + CryptoPP::AES::BLOCKSIZE, toReturn.end()), _keys[i].data(), iv.data());//return the decryption of the msg
	}
	return toReturn;//return the decryption of the msg
}

void PathManager::receiveThread()
{
	std::unique_lock<std::mutex> recvQueueLck(_recvQueueMtx, std::defer_lock);
	InterThreadMsg* tempMsg;
	byteVec tempVec;
	while (!_toShutDownThreads) {
		recvQueueLck.lock();
		_recvQueueCv.wait(recvQueueLck, [this] {return !_recvQueue.empty() || _toShutDownThreads; });

		if (!_recvQueue.empty()) {
			tempMsg = _recvQueue.front();
			_recvQueue.pop();
			recvQueueLck.unlock();
			handleInterThreadMsg(tempMsg);
		}
		else if (_toShutDownThreads) {//signaled to close
			recvQueueLck.unlock();
			break;
		}
		else {
			recvQueueLck.unlock();
		}
	}
}

void PathManager::handleNonGuardServerCrash(UnprocessedMsg msg)
{
	std::unique_lock<std::mutex> nodeStatusesLck(_nodeStatusesMtx);
	_nodeStatuses[msg.getMsg()[0]] = false;
	nodeStatusesLck.unlock();

	_directMsgsCv.notify_all();//make these throw if the path is bad.
	_createConnCV.notify_all();
	PathGenerator& pg = PathGenerator::instance();
	pg.reconnectNonGuardNode((NODE_NAMES)(msg.getMsg()[0]), this);

	nodeStatusesLck.lock();
	_nodeStatuses[msg.getMsg()[0]] = true;
	nodeStatusesLck.unlock();
	_nodeStatusesCv.notify_all();
}

void PathManager::handleInterThreadMsg(InterThreadMsg* msg)
{
	static int count = 0;
	PMConnComm* connStruct;
	byteVec tempVec;
	std::unique_lock<std::mutex> connsLck(_connsMtx, std::defer_lock);
	if (msg->getType() == "UnprocessedMsg") {
		UnprocessedMsg* unprMsg = (UnprocessedMsg*)msg;
		if (((UnprocessedMsg*)msg)->getFlag() == MSG_TYPES::CHECK_NEXT) {
			count++;
			std::cout << "recved: " << count << "\n";
			_checkGuardMsgsCout++;
		}
		else {
			byteVec decryptedMsg;
			if (unprMsg->getFlag() == MSG_TYPES::KEY_EXCHANGE && unprMsg->getOrigin() != NODE_NAMES::GUARD_NODE) {//if guard node, don't decrypt. If not guard node, encrypt with every encryption except for target's (since it is currently being negotiated)
				decryptedMsg = decrypt(unprMsg->getMsg(), (NODE_NAMES)((byte)unprMsg->getOrigin() - 1));
			}
			else if (unprMsg->getFlag() != MSG_TYPES::KEY_EXCHANGE) {//if not special circumstances, decrypt
				decryptedMsg = decrypt(unprMsg->getMsg(), (NODE_NAMES)((byte)unprMsg->getOrigin()));
			}
			if (unprMsg->getFlag() == MSG_TYPES::REGULAR) {
				int i = 0;
			}
			if (decryptedMsg.size() != 0) {
				if (unprMsg->getFlag() == MSG_TYPES::REGULAR) {
					int i = 0;
				}
				msg = new UnprocessedMsg(unprMsg->getOrigin(), unprMsg->getFlag(), unprMsg->getPaddingLen(),
					std::vector<byte>(decryptedMsg.begin(), decryptedMsg.end() - unprMsg->getPaddingLen()));
				delete unprMsg;
				unprMsg = (UnprocessedMsg*)msg;
			}
			
			if (unprMsg->getFlag() == MSG_TYPES::CREATE_CONN) {
				static std::vector<UnprocessedMsg> toSave;
				toSave.push_back(*unprMsg);
				ConnectionCreationReturnValue code = (ConnectionCreationReturnValue)unprMsg->getMsg()[0];
				std::lock_guard<std::mutex> createConnQueueLck(_createConnQueueMtx);
				if (code == ConnectionCreationReturnValue::SUCCESS) {
					byteVec unprocessedMsg = unprMsg->getMsg();
					_createConnQueue.push(new CreateConn(
						Utilities::bigEndianBytesToIntegerType<uint64_t>(byteVec(unprocessedMsg.begin() + 1, unprocessedMsg.end()))
					));
					_createConnCV.notify_all();
				}
				else {
					_createConnQueue.push(new InterThreadConnectionCreationError(code));
					_createConnCV.notify_all();
				}
			}
			else if (unprMsg->getFlag() == MSG_TYPES::SERVER_CRASH) {
				std::lock_guard<std::mutex> nonGuardNodeFixingThreadsLck(_nonGuardNodeFixingThreadsMtx);
				NON_GUARD_NODE_FIXING_THREADS temp = (NON_GUARD_NODE_FIXING_THREADS)(unprMsg->getMsg()[0] - 1);
				if (_nonGuardNodeFixingThreads[(byte)temp] != nullptr) {
					_nonGuardNodeFixingThreads[(byte)temp]->join();
					delete _nonGuardNodeFixingThreads[(byte)temp];
					_nonGuardNodeFixingThreads[(byte)temp] = nullptr;
				}
				_nonGuardNodeFixingThreads[(byte)temp] = new std::thread(&PathManager::handleNonGuardServerCrash, this, *(UnprocessedMsg*)msg);
			}
			else if (unprMsg->getFlag() == MSG_TYPES::PATH_GENERATION ||
				unprMsg->getFlag() == MSG_TYPES::KEY_EXCHANGE) {
				std::lock_guard<std::mutex> lck(_directMsgsMtx);
				_directMsgs.push(UnprocessedMsg(*unprMsg));
				_directMsgsCv.notify_all();
			}
			else {//conn closure or regular
				connsLck.lock();
				if (_conns.find(Utilities::bigEndianBytesToIntegerType<uint64_t>(unprMsg->getMsg())) == _conns.end()) {
					int i = Utilities::bigEndianBytesToIntegerType<uint64_t>(unprMsg->getMsg());
				}
				connStruct = _conns[Utilities::bigEndianBytesToIntegerType<uint64_t>(unprMsg->getMsg())];//getting the conn struct
				std::lock_guard<std::mutex> connReceivedMsgsLck(connStruct->receivedMtx);
				connsLck.unlock();
				if (unprMsg->getFlag() == MSG_TYPES::CONN_CLOSURE) {
					std::cout << "RECEIVED CONN CLOSURE\n";
					connStruct->received.push(new EndConnectionIngoing);
				}
				else {
					tempVec = unprMsg->getMsg();
					connStruct->received.push(new ReceivedMsg(byteVec(tempVec.begin() + ID_LEN, tempVec.end())));
				}
				connStruct->receivedCv.notify_all();
			}
		}
	}
	else {// (msg->getType() =="InterThreadCommunicationException"){
		_guardIsDown = true;
		_guardIsDownCV.notify_all();
	}
	delete msg;
}


void PathManager::ensureGuardNodeConnThread()
{
	int count = 0;
	std::unique_lock<std::mutex> guardIsDownLck(_guardIsDownMtx, std::defer_lock);
	byteVec vec;
	std::unique_lock<std::mutex> nodeStatusesLck(_nodeStatusesMtx, std::defer_lock);
	std::unique_lock<std::mutex> cmSendLck(_cmSendMtx, std::defer_lock);
	bool isThereConn = false;
	while (!_toShutDownThreads) {
		_checkGuardMsgsCout = 0;
		cmSendLck.lock();
		_cm.sendToServer(vec, 0, MSG_TYPES::CHECK_NEXT);
		cmSendLck.unlock();

		guardIsDownLck.lock();
		if (!_guardIsDownCV.wait_for(guardIsDownLck, std::chrono::seconds(SECS_TO_WAIT_QUICK), [this] { return _toShutDownThreads || _guardIsDown; })) {
			guardIsDownLck.unlock();
			std::cout << "checkguard count: " << _checkGuardMsgsCout << "b\n";
			isThereConn = _checkGuardMsgsCout != 0;
		}
		else if (_toShutDownThreads) {//signaled by dtor or guard replacment.
			break;
		}
		else {//signaled by recvThread
			guardIsDownLck.unlock();
			isThereConn = false;
		}

		if (!isThereConn) {
			if (!Utilities::isIPV4InternetConnectionOk()) {
				continue;
			}
			else {
				nodeStatusesLck.lock();
				_nodeStatuses[0] = false;
				nodeStatusesLck.unlock();
				//notifing all recvs that the guard crashes. (they will know it is the guard becuase  _isPathNotOk = false)
				_directMsgsCv.notify_all();
				_createConnCV.notify_all();
				PathGenerator& pg = PathGenerator::instance();
				pg.reconnectGuardNode(this);
				nodeStatusesLck.lock();
				_nodeStatuses[0] = true;
				nodeStatusesLck.unlock();
				_nodeStatusesCv.notify_all();
			}
		}
	}
}

void PathManager::checkNodeCrash(NODE_NAMES fixingNode)
{
	if (fixingNode == NODE_NAMES::NO_NODE) {
		return;
	}
	std::lock_guard<std::mutex> lck(_nodeStatusesMtx);
	for (byte i = 0; i <= (byte)NODE_NAMES::EXIT_NODE; i++) {
		if (!_nodeStatuses[i] && i < (byte)fixingNode) { //node is down. AND shouldn't be overriden.
			throw NodeCrashException();
		}
	}
}


PathManager::~PathManager()
{
	_toShutDownThreads = true;
	_recvQueueCv.notify_all();//notifying the threads it should shut down.
	_guardIsDownCV.notify_all();
	std::lock_guard<std::mutex> threadsLck(_threadsMtx);
	_threads[(byte)THREADS::RECV_THREAD]->join();
	delete _threads[(byte)THREADS::RECV_THREAD];
	_threads[(byte)THREADS::RECV_THREAD] = nullptr;

	_threads[(byte)THREADS::ENSURE_GUARD_CONN_THREAD]->join();
	delete _threads[(byte)THREADS::ENSURE_GUARD_CONN_THREAD];
	_threads[(byte)THREADS::ENSURE_GUARD_CONN_THREAD] = nullptr;

	std::lock_guard<std::mutex> cmSendLck(_cmSendMtx);
	_cm.sendToServer({}, 0, MSG_TYPES::PATH_CLOSURE, NODE_NAMES::EXIT_NODE);
	_cm.closeCommunication();
	//after communication has been closed, make sure all queues we are responsible for deleting their msgs are clean.
	cleanQueue(_recvQueue);
	cleanQueue(_createConnQueue);
}

void PathManager::waitForPathToBeOk(bool waitForFix )
{
	std::unique_lock<std::mutex> nodeStatusesLck(_nodeStatusesMtx);
	_nodeStatusesCv.wait(nodeStatusesLck, [this, waitForFix] { return waitForFix || getIsPathOk(); });
}

void PathManager::waitForPathToBeOk(std::unique_lock<std::mutex>& lck, bool waitForFix )
{
	_nodeStatusesCv.wait(lck, [this, waitForFix] { return waitForFix || getIsPathOk(); });
}

void PathManager::cleanQueue(std::queue<InterThreadMsg*> queue)
{
	while (!queue.empty()) {
		delete queue.front();
		queue.pop();
	}
}

void PathManager::setNodeDetails(NODE_NAMES node, byteVec ip, byteVec token)
{
	std::lock_guard<std::mutex> lck(_nodesDetailsMtx);
	if (node == NODE_NAMES::GUARD_NODE) {
		_guardIp = ip;
	}
	else if (node == NODE_NAMES::MID_NODE) {
		_midIp = ip;
		_midToken = token;
	}
	else {
		_exitIp = ip;
		_exitToken = token;
	}
}

std::pair<byteVec, byteVec> PathManager::getNodeDetails(NODE_NAMES node)
{
	std::lock_guard<std::mutex> lck(_nodesDetailsMtx);
	std::pair<byteVec, byteVec> ipAndToken;
	if (node == NODE_NAMES::GUARD_NODE) {
		if (_guardIp.size() == 0) {
			throw UninitiatedValue();
		}
		ipAndToken.first = _guardIp;
	}
	else if (node == NODE_NAMES::MID_NODE) {
		if (_midIp.size() == 0 ||_midToken.size() == 0) {
			throw UninitiatedValue();
		}		
		ipAndToken.second = _midToken;
		ipAndToken.first= _midIp;
	}
	else {
		if (_exitIp.size() == 0 || _exitToken.size() == 0) {
			throw UninitiatedValue();
		}
		ipAndToken.second = _exitToken;
		ipAndToken.first= _exitIp;
	}
	return ipAndToken;
}

byteVec PathManager::connectToNewGuardNode(byteVec ip)
{
	if (ip.size() == 0) {
		ip = DBManager::instance().getGuardNode().second;
	}
	std::unique_lock<std::mutex> cmSendLck(_cmSendMtx, std::defer_lock);
	std::unique_lock<std::mutex> threadsLck(_threadsMtx, std::defer_lock);
	bool hasConnected = false;
	
	_toShutDownThreads = true;
	_recvQueueCv.notify_all();
	threadsLck.lock();
	_threads[(byte)THREADS::RECV_THREAD]->join();
	delete _threads[(byte)THREADS::RECV_THREAD];
	_threads[(byte)THREADS::RECV_THREAD] = nullptr;
	threadsLck.unlock();
	_toShutDownThreads = false;//prevent the new threads from shutting down.
	cmSendLck.lock();
	_cm.closeCommunication();
	_cm.setCommunicationDetails(ip);
	while (!hasConnected) {
		try {
			_cm.connectToServer();
			hasConnected = true;
		}
		catch (CommunicationException) {
			if (Utilities::isIPV4InternetConnectionOk()){
				_cm.closeCommunication();
				_cm.setCommunicationDetails(ip);
			}
		}
	}

	threadsLck.lock();
	_threads[(byte)THREADS::RECV_THREAD] = new std::thread(&PathManager::receiveThread, this);
	return ip;
}

bool PathManager::getIsPathOk()
{
	return _nodeStatuses[(byte)NODE_NAMES::GUARD_NODE] && _nodeStatuses[(byte)NODE_NAMES::MID_NODE] && _nodeStatuses[(byte)NODE_NAMES::EXIT_NODE];
}

void PathManager::sendMsgDirectly(byteVec msg, MSG_TYPES type, NODE_NAMES dst , NODE_NAMES overridePathEnsurance)
{
	byte paddingLen =0;
	checkNodeCrash(overridePathEnsurance);
	std::unique_lock<std::mutex> toGuardRecvQueue(_directMsgsMtx);
	while (!_directMsgs.empty()) {
		_directMsgs.pop();
	}
	toGuardRecvQueue.unlock();
	std::vector<byte> finalMessage;
	if (type == MSG_TYPES::KEY_EXCHANGE && dst == NODE_NAMES::GUARD_NODE) {//if the msg is a key exchange msg and talking to guard, don't encrypt
		finalMessage = msg;
		paddingLen = 0;
	}
	else if (type == MSG_TYPES::KEY_EXCHANGE) {//if msg is key exchange and not talking to guard, encrypt without last node's encryption
		finalMessage = encrypt(msg, (NODE_NAMES)((byte)dst - 1));
		paddingLen = (CryptoPP::AES::BLOCKSIZE - (msg.size() % CryptoPP::AES::BLOCKSIZE)) % CryptoPP::AES::BLOCKSIZE;
	}
	else {//if not a special case, encrypt message
		finalMessage = encrypt(msg, dst);
		paddingLen = (CryptoPP::AES::BLOCKSIZE - (msg.size() % CryptoPP::AES::BLOCKSIZE)) % CryptoPP::AES::BLOCKSIZE;
	}
	_cm.sendToServer(finalMessage, paddingLen, type, dst);

}



byteVec PathManager::recvPathGeneration(int timeoutMilliSec, NODE_NAMES overridePathEnsurance)
{
	byteVec msg;
	std::unique_lock<std::mutex> pathGenerationMsgsLck(_directMsgsMtx, std::defer_lock);
	checkNodeCrash(overridePathEnsurance);
	pathGenerationMsgsLck.lock();
	if (_directMsgs.size() != 0) {
		msg = _directMsgs.front().getMsg();
		_directMsgs.pop();
		pathGenerationMsgsLck.unlock();
		return msg;
	}
	else{
		if (timeoutMilliSec == NO_TIMEOUT) {
			_directMsgsCv.wait(pathGenerationMsgsLck,
				[this] { return std::find(_nodeStatuses.begin(), _nodeStatuses.end(), false) != _nodeStatuses.end() || !_directMsgs.empty(); });
			if (_directMsgs.empty()) {// means that the guard conn thread notified or the recv handler.
				throw NodeCrashException();
			}
		}
		else {
			if (!_directMsgsCv.wait_for(pathGenerationMsgsLck, std::chrono::milliseconds(timeoutMilliSec),
				[this] { return std::find(_nodeStatuses.begin(), _nodeStatuses.end(), false) != _nodeStatuses.end() || !_directMsgs.empty(); })) {
				pathGenerationMsgsLck.unlock();
				throw TimeOut();
			}
			else if (_directMsgs.empty()) {// means that the guard conn thread notified or the recv handler.
				throw NodeCrashException();
			}
		}
		msg = _directMsgs.front().getMsg();
		_directMsgs.pop();
		return msg;
	}
} 

void PathManager::setIsPrimary(bool value)
{
	int numOfConns = 0;
	std::unique_lock<std::mutex> connsLck(_connsMtx);
	numOfConns = (int)_conns.size();
	connsLck.unlock();
	if (!numOfConns && !value) {
		_isPrimary = value;
		delete this;
	}
	else {
		_isPrimary=value;
	}

}

PathManager::PathManager(byteVec ip)
	:_cm(_recvQueueMtx, _recvQueueCv, _recvQueue)
{
	_cm.setCommunicationDetails(ip);
	_cm.connectToServer();
	std::lock_guard<std::mutex> lck(_threadsMtx);
	_threads[(byte)THREADS::RECV_THREAD] = new std::thread(&PathManager::receiveThread, this);
	_threads[(byte)THREADS::ENSURE_GUARD_CONN_THREAD] = new std::thread(&PathManager::ensureGuardNodeConnThread, this);
}

uint64_t PathManager::sendRecvConnCreationMsg(byteVec ipAndPort)
{
	uint64_t res = 0;
	byteVec encryptedMsg;
	std::unique_lock<std::mutex> createConnQueueLck(_createConnQueueMtx, std::defer_lock);
	std::unique_lock<std::mutex> cmSendLck(_cmSendMtx, std::defer_lock);
	waitForPathToBeOk(); //will not wait if already ok.
	createConnQueueLck.lock();
	while (_createConnQueue.size() != 0) {
		delete _createConnQueue.front();
		_createConnQueue.pop();
	}
	createConnQueueLck.unlock();
	cmSendLck.lock();
	encryptedMsg = encrypt(ipAndPort, NODE_NAMES::EXIT_NODE);
	_cm.sendToServer(encryptedMsg, CryptoPP::AES::BLOCKSIZE - ipAndPort.size(), MSG_TYPES::CREATE_CONN);
	cmSendLck.unlock();
	createConnQueueLck.lock();
	if (_createConnQueue.empty()) {
		if (!_createConnCV.wait_for(createConnQueueLck, std::chrono::seconds(SECS_TO_WAIT), [this] {return !_createConnQueue.empty() || std::find(_nodeStatuses.begin(), _nodeStatuses.end(), false) != _nodeStatuses.end(); })) {
			throw TimeOut();
		}
		else if (_createConnQueue.empty()) {//means notified by the guardConn thread or recv handler thread.
			throw NodeCrashException();
		}
	}
	if (_createConnQueue.front()->getType() == "CreateConn") {
		res = ((CreateConn*)_createConnQueue.front())->getId();
		delete _createConnQueue.front();
		_createConnQueue.pop();
	}
	else {
		ConnectionCreationReturnValue code = ((InterThreadConnectionCreationError*)_createConnQueue.front())->getErrorCode();
		delete _createConnQueue.front();
		_createConnQueue.pop();
		throw ConnectionCreationError(code);
	}
	return res;
}

PMConnComm* PathManager::addConnection(uint64_t id)
{
	std::lock_guard<std::mutex> lck(_connsMtx);
	PMConnComm* connStruct = new PMConnComm{this};
	_conns[id] = connStruct;
	return _conns[id];
}

void PathManager::setKey(byteVec key, NODE_NAMES node)
{
	unqLck lck(_keysMtx);
	std::copy(key.begin(), key.end(), _keys[(byte)node].begin());
}
