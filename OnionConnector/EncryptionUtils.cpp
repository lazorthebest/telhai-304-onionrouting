#include "EncryptionUtils.h"

CryptoPP::Integer EncryptionUtils::getMod()
{
	using namespace CryptoPP;
	byte modBytes[DH_KEY_LEN] = { 255, 255, 255, 255, 255, 255, 255, 255, 201, 15,
		218, 162, 33, 104, 194, 52, 196, 198, 98, 139, 128, 220, 28,
		209, 41, 2, 78, 8, 138, 103, 204, 116, 2, 11, 190, 166, 59,
		19, 155, 34, 81, 74, 8, 121, 142, 52, 4, 221, 239, 149, 25,
		179, 205, 58, 67, 27, 48, 43, 10, 109, 242, 95, 20, 55, 79,
		225, 53, 109, 109, 81, 194, 69, 228, 133, 181, 118, 98, 94,
		126, 198, 244, 76, 66, 233, 166, 55, 237, 107, 11, 255, 92,
		182, 244, 6, 183, 237, 238, 56, 107, 251, 90, 137, 159, 165,
		174, 159, 36, 17, 124, 75, 31, 230, 73, 40, 102, 81, 236, 228,
		91, 61, 194, 0, 124, 184, 161, 99, 191, 5, 152, 218, 72, 54, 28,
		85, 211, 154, 105, 22, 63, 168, 253, 36, 207, 95, 131, 101, 93,
		35, 220, 163, 173, 150, 28, 98, 243, 86, 32, 133, 82, 187, 158,
		213, 41, 7, 112, 150, 150, 109, 103, 12, 53, 78, 74, 188, 152, 4
		, 241, 116, 108, 8, 202, 24, 33, 124, 50, 144, 94, 70, 46, 54, 206,
		59, 227, 158, 119, 44, 24, 14, 134, 3, 155, 39, 131, 162, 236, 7,
		162, 143, 181, 197, 93, 240, 111, 76, 82, 201, 222, 43, 203,
		246, 149, 88, 23, 24, 57, 149, 73, 124, 234, 149, 106, 229,
		21, 210, 38, 24, 152, 250, 5, 16, 21, 114, 142, 90, 138, 172,
		170, 104, 255, 255, 255, 255, 255, 255, 255, 255 };
	Integer p(modBytes, DH_KEY_LEN);
	return p;
}

std::vector<CryptoPP::byte> EncryptionUtils::generateKey(CryptoPP::byte toHash[DH_KEY_LEN])
{
	using namespace CryptoPP;
	std::vector<byte> res;
	res.resize(SHA256::DIGESTSIZE, 0);
	SHA256 hash;
	hash.Update(toHash, DH_KEY_LEN);
	hash.Final(res.data());
	return res;
}

std::vector<CryptoPP::byte> EncryptionUtils::generateRand(size_t numBytes)
{
	using namespace CryptoPP;
	AutoSeededRandomPool randGenerator;
	byte* res = new byte[numBytes];
	randGenerator.GenerateBlock(res, numBytes);
	std::vector<CryptoPP::byte> resVec(res, res + numBytes);
	delete[] res;
	return resVec;
}

std::vector<CryptoPP::byte> EncryptionUtils::encrypt(std::vector<CryptoPP::byte> msg,
	CryptoPP::byte key[CryptoPP::AES::MAX_KEYLENGTH],
	CryptoPP::byte iv[CryptoPP::AES::BLOCKSIZE])
{
	using namespace CryptoPP;
	while (msg.size() % AES::BLOCKSIZE != 0) {//padding
		msg.push_back(0);
	}
	std::vector<byte> cipher(msg.size(), 0);
	CBC_Mode< AES >::Encryption encryption;
	encryption.SetKeyWithIV(key, AES::MAX_KEYLENGTH, iv);
	ArraySource vs(msg.data(), msg.size(), true,
		new StreamTransformationFilter(encryption,
			new ArraySink(cipher.data(), cipher.size()),
			StreamTransformationFilter::NO_PADDING)
	);
	return cipher;
}

std::vector<CryptoPP::byte> EncryptionUtils::decrypt(std::vector<CryptoPP::byte> cipher,
	CryptoPP::byte key[CryptoPP::AES::MAX_KEYLENGTH],
	CryptoPP::byte iv[CryptoPP::AES::BLOCKSIZE])
{
	using namespace CryptoPP;
	std::vector<CryptoPP::byte> decrypted(cipher.size(), 0);
	CryptoPP::AES::Decryption aesDecryption(key, CryptoPP::AES::MAX_KEYLENGTH);
	CryptoPP::CBC_Mode_ExternalCipher::Decryption cbcDecryption(aesDecryption, iv);
	CryptoPP::StreamTransformationFilter stfDecryptor(cbcDecryption, new ArraySink(decrypted.data(), decrypted.size()), StreamTransformationFilter::ZEROS_PADDING);
	stfDecryptor.Put(reinterpret_cast<const unsigned char*>(cipher.data()), cipher.size());
	stfDecryptor.MessageEnd();
	return decrypted;
	//	using namespace CryptoPP;
	//	while (cipher.size() % AES::BLOCKSIZE != 0) {//padding
	//		cipher.push_back(0);
	//	}
	//	std::vector<byte> plainText;
	//	CBC_Mode< AES >::Encryption decryption;
	//	decryption.SetKeyWithIV(key, AES::MAX_KEYLENGTH, iv);
	//	VectorSource vs(cipher, true,
	//		new StreamTransformationFilter(decryption,
	//			new VectorSink(plainText),
	//			StreamTransformationFilter::NO_PADDING)
	//	);
	//	return plainText;
}

std::vector<CryptoPP::byte> EncryptionUtils::power(CryptoPP::byte base[DH_KEY_LEN], CryptoPP::byte exponent[PRIVATE_EXPONENT_LEN])
{
	using namespace CryptoPP;
	byte res[DH_KEY_LEN] = { 0 };
	Integer baseInt(base, DH_KEY_LEN);
	Integer exponentInt(exponent, PRIVATE_EXPONENT_LEN);
	Integer resInt = a_exp_b_mod_c(baseInt, exponentInt, getMod());
	resInt.Encode(res, DH_KEY_LEN);
	return std::vector<byte>(res, res + DH_KEY_LEN);
}

std::vector<CryptoPP::byte> EncryptionUtils::power2(CryptoPP::byte exponent[PRIVATE_EXPONENT_LEN])
{
	using namespace CryptoPP;
	byte base[DH_KEY_LEN] = { 0 };
	base[DH_KEY_LEN - 1] = 2;
	return power(base, exponent);
}


//#include "EncryptionUtils.h"
//
//CryptoPP::Integer EncryptionUtils::getMod()
//{
//	using namespace CryptoPP;
//	byte modBytes[DH_KEY_LEN] = { 255, 255, 255, 255, 255, 255, 255, 255, 201, 15,
//		218, 162, 33, 104, 194, 52, 196, 198, 98, 139, 128, 220, 28,
//		209, 41, 2, 78, 8, 138, 103, 204, 116, 2, 11, 190, 166, 59,
//		19, 155, 34, 81, 74, 8, 121, 142, 52, 4, 221, 239, 149, 25,
//		179, 205, 58, 67, 27, 48, 43, 10, 109, 242, 95, 20, 55, 79,
//		225, 53, 109, 109, 81, 194, 69, 228, 133, 181, 118, 98, 94,
//		126, 198, 244, 76, 66, 233, 166, 55, 237, 107, 11, 255, 92,
//		182, 244, 6, 183, 237, 238, 56, 107, 251, 90, 137, 159, 165,
//		174, 159, 36, 17, 124, 75, 31, 230, 73, 40, 102, 81, 236, 228,
//		91, 61, 194, 0, 124, 184, 161, 99, 191, 5, 152, 218, 72, 54, 28,
//		85, 211, 154, 105, 22, 63, 168, 253, 36, 207, 95, 131, 101, 93,
//		35, 220, 163, 173, 150, 28, 98, 243, 86, 32, 133, 82, 187, 158,
//		213, 41, 7, 112, 150, 150, 109, 103, 12, 53, 78, 74, 188, 152, 4
//		, 241, 116, 108, 8, 202, 24, 33, 124, 50, 144, 94, 70, 46, 54, 206,
//		59, 227, 158, 119, 44, 24, 14, 134, 3, 155, 39, 131, 162, 236, 7,
//		162, 143, 181, 197, 93, 240, 111, 76, 82, 201, 222, 43, 203,
//		246, 149, 88, 23, 24, 57, 149, 73, 124, 234, 149, 106, 229,
//		21, 210, 38, 24, 152, 250, 5, 16, 21, 114, 142, 90, 138, 172,
//		170, 104, 255, 255, 255, 255, 255, 255, 255, 255 };
//	Integer p(modBytes, DH_KEY_LEN);
//	return p;
//}
//
//std::vector<CryptoPP::byte> EncryptionUtils::generateKey(CryptoPP::byte toHash[DH_KEY_LEN])
//{
//	using namespace CryptoPP;
//	std::vector<byte> res;
//	res.resize(SHA256::DIGESTSIZE, 0);
//	SHA256 hash;
//	hash.Update(toHash, DH_KEY_LEN);
//	hash.Final(res.data());
//	return res;
//}
//
//std::vector<CryptoPP::byte> EncryptionUtils::generateRand(size_t numBytes)
//{
//	using namespace CryptoPP;
//	AutoSeededRandomPool randGenerator;
//	byte* res = new byte[numBytes];
//	randGenerator.GenerateBlock(res, numBytes);
//	std::vector<CryptoPP::byte> resVec(res, res + numBytes);
//	delete[] res;
//	return resVec;
//}
//
//std::vector<CryptoPP::byte> EncryptionUtils::encrypt(std::vector<CryptoPP::byte> msg, 
//	CryptoPP::byte key[CryptoPP::AES::MAX_KEYLENGTH],
//	CryptoPP::byte iv[CryptoPP::AES::BLOCKSIZE])
//{
//	using namespace CryptoPP;
//	while (msg.size() % AES::BLOCKSIZE != 0) {//padding
//		msg.push_back(0);
//	}
//	std::vector<byte> cipher;
//	CBC_Mode< AES >::Encryption encryption;
//	encryption.SetKeyWithIV(key, AES::MAX_KEYLENGTH, iv);
//	VectorSource vs(msg, true,
//		new StreamTransformationFilter(encryption,
//			new VectorSink(cipher),
//			StreamTransformationFilter::NO_PADDING)   
//	); 
//	return cipher;
//}
//
//std::vector<CryptoPP::byte> EncryptionUtils::decrypt(std::vector<CryptoPP::byte> cipher,
//	CryptoPP::byte key[CryptoPP::AES::MAX_KEYLENGTH], 
//	CryptoPP::byte iv[CryptoPP::AES::BLOCKSIZE])
//{
//	using namespace CryptoPP;
//	while (cipher.size() % AES::BLOCKSIZE != 0) {//padding
//		cipher.push_back(0);
//	}
//	std::vector<CryptoPP::byte> decrypted;
//	CryptoPP::AES::Decryption aesDecryption(key, CryptoPP::AES::MAX_KEYLENGTH);
//	CryptoPP::CBC_Mode_ExternalCipher::Decryption cbcDecryption(aesDecryption, iv);
//
//	CryptoPP::StreamTransformationFilter stfDecryptor(cbcDecryption, new VectorSink(decrypted), StreamTransformationFilter::NO_PADDING);
//	stfDecryptor.Put(reinterpret_cast<const unsigned char*>(cipher.data()), cipher.size());
//	stfDecryptor.MessageEnd();
//	return decrypted;
//
////	while (cipher.size() % AES::BLOCKSIZE != 0) {//padding
////		cipher.push_back(0);
////	}
////	std::vector<byte> plainText;
////	CBC_Mode< AES >::Encryption decryption;
////	decryption.SetKeyWithIV(key, AES::MAX_KEYLENGTH, iv);
////	VectorSource vs(cipher, true,
////		new StreamTransformationFilter(decryption,
////			new VectorSink(plainText),
////			StreamTransformationFilter::NO_PADDING)
////	); 	return plainText;
//}
//
//std::vector<CryptoPP::byte> EncryptionUtils::power(CryptoPP::byte base[DH_KEY_LEN], CryptoPP::byte exponent[PRIVATE_EXPONENT_LEN])
//{
//	using namespace CryptoPP;
//	byte res[DH_KEY_LEN] = { 0 };
//	Integer baseInt(base, DH_KEY_LEN);
//	Integer exponentInt(exponent, PRIVATE_EXPONENT_LEN);
//	Integer resInt = a_exp_b_mod_c(baseInt, exponentInt, getMod());
//	resInt.Encode(res, DH_KEY_LEN);
//	return std::vector<byte>(res, res + DH_KEY_LEN);
//}
//
//std::vector<CryptoPP::byte> EncryptionUtils::power2(CryptoPP::byte exponent[PRIVATE_EXPONENT_LEN])
//{
//	using namespace CryptoPP;
//	byte base[DH_KEY_LEN] = { 0 };
//	base[DH_KEY_LEN - 1] = 2;
//	return power(base, exponent);
//}
