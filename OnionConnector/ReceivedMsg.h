#pragma once
#include <vector>
#include "InterThreadMsg.h"
typedef unsigned char byte;
class ReceivedMsg : public InterThreadMsg
{
	std::vector<byte> _msg;
public:
	ReceivedMsg(std::vector<byte> msg) {
		_msg = msg;
	}
	virtual std::string getType() {
		return "ReceivedMsg";
	}
	std::vector<byte> getMsg() {
		return _msg;
	}
	virtual ~ReceivedMsg() {};
};

