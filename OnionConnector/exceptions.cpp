#pragma once
#include <iostream>
#include "InterThreadMsg.h"
#define USERNAME_EXISTS 0
#define UNINITIATED_CONNECTION 1
#define COMMUNICATION_EXCEPTION 2
#define CLOSE_DB 3
#define NO_COOKIE_FOUND 4
#define SQLITE_FAILURE 5
#define AUTH_FAILURE 6
#define EXCEEDING_PENDING_REQUESTS_LIMIT 7
#define LOGIN_DENIED 8
#define ERROR_SPECIFIER_LOC 1
#define MESSEGE_STARTS_WITH 2
typedef unsigned char byte;
class GeneralException :public std::exception {
	std::string _errorMsg = "";
public:
	GeneralException(std::string error)
	{
		_errorMsg = error;
	};
	std::string getError() { return _errorMsg; };
};

enum class ConnectionCreationReturnValue {
	SUCCESS,
	SOCKET_INITIALIZATION_FAILED,
	INVALID_IP,
	CONNECTION_FAILED
};


class InterThreadConnectionCreationError : public InterThreadMsg {
	ConnectionCreationReturnValue _val;
public:
	InterThreadConnectionCreationError(ConnectionCreationReturnValue val) {
		_val = val;
	}
	ConnectionCreationReturnValue getErrorCode() { return _val; };

	std::string getType() { return "InterThreadConnectionCreationError"; };
};

class ConnectionCreationError : public std::exception {
	ConnectionCreationReturnValue _val;
public:
	ConnectionCreationError(ConnectionCreationReturnValue val) {
		_val = val;
	}
	ConnectionCreationError(InterThreadConnectionCreationError interThreadError) {
		_val = interThreadError.getErrorCode();
	}
	ConnectionCreationReturnValue getErrorCode() { return _val; };
};

class NoGuardsAvailException :public std::exception {
};

class NodeCrashException:public std::exception {
};

class LackingBytes :public std::exception {

};

class InvalidMsgLen : public GeneralException {
public:
	InvalidMsgLen(std::string error)
		:GeneralException(error)
	{
	}
};

class MiscException :public GeneralException {
public:
	MiscException(std::string error)
		:GeneralException(error)
	{
	}
};


class UninitiatedValue :public std::exception {
};

//thrown if connectToServer was not called in Communicator.
class UninitiatedConnection :public UninitiatedValue {
};

class UnansweredEndConnection : public std::exception {
};

class InterThreadUninitiatedConnection : public UninitiatedConnection, public InterThreadMsg {
public:
	virtual std::string getType() { return "InterThreadUninitiatedConnection"; };
	virtual ~InterThreadUninitiatedConnection() {};
};

class CommunicationException {
	int _code = 0;
	std::string _errorMsg;
public:
	CommunicationException(std::string errorMsg, int errorCode)
	{
		_errorMsg = errorMsg;
		_code = errorCode;
	};
	virtual ~CommunicationException() {
		std::cout << "In CommException dtor\n";
	};
	std::string getErrorMsg() { return _errorMsg; };
	int getErrorCode() { return _code; };
};

class InterThreadCommunicationException : public InterThreadMsg {
	int _code;
	std::string _msg;
public:
	InterThreadCommunicationException(CommunicationException cmExc)
	{
		_msg = cmExc.getErrorMsg();
		_code = cmExc.getErrorCode();
	}
	virtual ~InterThreadCommunicationException() {
		std::cout << "IN\n";
	};
	std::string getErrorMsg() { return _msg; };
	int getErrorCode() { return _code; };
	virtual std::string getType() { return "InterThreadCommunicationException"; };
};

class TimeOut :public std::exception {

};


class CloseDBException :public std::exception {
	std::string errorMsg = "";
public:
	CloseDBException()
	{
		errorMsg = "You must open the db before accessing data!";
	};
	CloseDBException(std::string error)
	{
		errorMsg = error;
	};

	std::string getError() { return errorMsg; };
};

class NoCookieFoundException :public std::exception {
	std::string errorMsg = "";
public:
	NoCookieFoundException()
	{
		errorMsg = "No cookie was found in the DB";
	};
	NoCookieFoundException(std::string error)
	{
		errorMsg = error;
	};

	std::string getError() { return errorMsg; };
};

class SqliteFailureExcpetion :public GeneralException {
public:
	SqliteFailureExcpetion(std::string error)
		:GeneralException(error)
	{
	}
};