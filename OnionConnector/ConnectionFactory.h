#pragma once
#include <mutex>
#include <condition_variable>
#include <vector>
#include <thread>
#include "PathManager.h"
#include "Utilities.h"
#include "PathGenerator.h"
#include "Connection.h"
#include "PMCFBridger.h"

#define SEC_IN_MILLI 1000
#define CHANGE_PM_TIME 1

class ConnectionFactory
{
	std::mutex _changePMMtx;
	PathManager* _currPM;
	std::atomic<bool> _toTerminate;
	std::mutex _stopWaitingMtx;
	bool _updatePMNow = false, _updatedPMNow = false;
	std::condition_variable _stopWaitingCV;
	std::thread PMUpdater;

	void updatePM();//thread that updates PM every ten minutes
	ConnectionFactory();//THROWS noGuardsAvailException. ctor and dtor private, so that it is singleton
	~ConnectionFactory();
public:
	//changes PM once and resets the replacment timer.
	void updatePm();
	static ConnectionFactory& instance();//THROWS noGuardsAvailException. Gets the singleton
	//THROWS ConnectionCreationError
	//THROWS UninitiatedConnection. Converts the IP to a vector of bytes, then passes it on to the other createConnection function
	Connection* createConnection(std::string IP, uint16_t port, std::function<void(void*, ReceivedMsg, bool)> callback, void* data);
	//THROWS ConnectionCreationError
	//THROWS UninitiatedConnection. Creates a connection object. The returned object must be deallocated.
	Connection* createConnection(std::vector<unsigned char> IP, uint16_t port, std::function<void(void*, ReceivedMsg, bool)> callback, void* data);
};
