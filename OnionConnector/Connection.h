#pragma once
#include <queue>
#include <vector>
#include <mutex>
#include <thread>
#include <functional>
#include <condition_variable>
#include "ReceivedMsg.h"
#include "ToSendMsg.h"
#include "exceptions.cpp"
#include "ConnTerminated.h"
#include "PathManager.h"



#define MAX_TRIES_SEND 5
#define MS_TO_S 1000

typedef unsigned char byte;

class Connection 
{
	std::mutex _endConnectionMsgsMtx;
	bool _isTerminated = false;
	bool _isClosed = false;
	byteVec _id;
	std::condition_variable _stopRecvCV;//signals that _stopReceiving is now true
	PMConnComm* _comm;//communicator with PM
	std::atomic<bool> _stopReceiving = false;//false until the connection should be ended, signals that receiving connection ended
	std::function<void(void* dataFromOrigin, ReceivedMsg, bool isAlive)> _callback;//the callback function that should be run on every received message
	std::thread _receiveMsgsThread;//the thread that receives messages from the user and calls the callback on them
	void* _data;//data for callback function

	void receiveMsgs();//thread that receives messages and passes them on to callback
public:
	Connection(PMConnComm* comm, std::function<void(void* dataFromOrigin, ReceivedMsg, bool isAlive)> callback, void* data, uint64_t id);//ctor, takes in PMConnComm, and the callback as well as the variable to be passes to it
	~Connection();
	void sendMsg(std::vector<byte> toSend);//THROWS UninitiatedConnection if the connection is closed. Sends a message
	void sendMsg(std::string toSend);//THROWS UninitiatedConnection if the connection is closed. Converts the string to bytes, then passes it on to the other sendMsg
	void endConnection();//Ends the Connection object
};

