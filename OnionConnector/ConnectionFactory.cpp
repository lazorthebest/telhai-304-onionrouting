#include "ConnectionFactory.h"


void ConnectionFactory::updatePM()
{
	std::unique_lock<std::mutex> stopWaitingLck(_stopWaitingMtx, std::defer_lock);
	while (true) {
		stopWaitingLck.lock();
		_stopWaitingCV.wait_for(stopWaitingLck, std::chrono::seconds(10), [this] {return _toTerminate || _updatePMNow; });//TODO change this
		if (_toTerminate) {
			return;
		}
		else {
			_updatePMNow = false;
		}
		stopWaitingLck.unlock();

		std::unique_lock<std::mutex> changePMLock(_changePMMtx);
		PathGenerator& pg = PathGenerator::instance();
		_currPM = pg.generatePath();//gets a new PM
		_updatedPMNow = true;
		_stopWaitingCV.notify_all();
	}
}

ConnectionFactory::ConnectionFactory()
{
	std::cout << "2\n";
	DBManager::instance();//ensuring DB is alive before calling PG which depends on it (unless given DSIPs)
	std::cout << "2\n";
	PathGenerator& pg = PathGenerator::instance();
	std::cout << "2\n";
	_currPM = pg.generatePath();
	std::cout << "2\n";
	PMUpdater = std::thread(&ConnectionFactory::updatePM, this);//starts the UpdatePM thread
	std::cout << "2\n";
}

ConnectionFactory::~ConnectionFactory()
{
	_toTerminate = true;
	_stopWaitingCV.notify_all();//notifies the thread, then waits for it to stop
	PMUpdater.join();
	std::lock_guard<std::mutex> lck(_changePMMtx);
	PMCFBridger::dtorPathManager(_currPM);
}

void ConnectionFactory::updatePm()
{
	std::unique_lock<std::mutex> stopWaitingLck(_stopWaitingMtx);
	_updatePMNow = true;
	_stopWaitingCV.notify_all();
	_stopWaitingCV.wait(stopWaitingLck, [this] {return _updatedPMNow; });//waiting for the PM to be ready.
	_updatedPMNow = false;
}

ConnectionFactory& ConnectionFactory::instance()
{
	static ConnectionFactory cf;
	return cf;
};

Connection* ConnectionFactory::createConnection(std::string IP, uint16_t  port, std::function<void(void*, ReceivedMsg, bool)> callback, void* data)
{
	std::vector<std::string> destIPStr = Utilities::split(IP, ".");
	std::vector<unsigned char> destIP;
	std::transform(destIPStr.begin(), destIPStr.end(), std::back_inserter(destIP),
		[](const std::string& str) { return (byte)std::stoi(str); });
	return createConnection(destIP, port, callback, data);
}

Connection* ConnectionFactory::createConnection(std::vector<byte> IP, uint16_t port, std::function<void(void*, ReceivedMsg, bool)> callback, void* data)
{
	byteVec toSend;
	uint64_t id;
	byteVec portBytes = Utilities::ShortIntTobigEndianBytes(port);
	std::unique_lock<std::mutex> changePMLock(_changePMMtx, std::defer_lock);
	toSend.insert(toSend.end(), IP.begin(), IP.end());
	toSend.insert(toSend.end(), portBytes.begin(), portBytes.end());
	while (true) {
		changePMLock.lock();
		try {
			id = _currPM->sendRecvConnCreationMsg(toSend);
			changePMLock.unlock();
			break;
		}
		catch (TimeOut exc) {
			_currPM->waitForPathToBeOk();
			changePMLock.unlock();
		}
		catch (CommunicationException exc) {
			_currPM->waitForPathToBeOk();
			changePMLock.unlock();
		}
		catch (NodeCrashException exc) {
			_currPM->waitForPathToBeOk();
			changePMLock.unlock();
		}
	}
	return new Connection(_currPM->addConnection(id), callback, data,id);
}

