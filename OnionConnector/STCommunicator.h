#pragma once
#include "ICommunicator.h"
#define BUFF_LEN 1024
#define LEN_DESCRIPTION_LEN 2
#define DS_PORT 43984

class STCommunicator:public ICommunicator
{
	WSAEVENT _readEvent = nullptr;
public:
	virtual void closeCommunication();
	//THROWS Communication Exception
	virtual void connectToServer();
	void setCommunicationDetails(std::vector<byte> ip);
	void sendToServer( std::vector<byte> toSend, byte flags = 0) ;
	//will not return the  bytes describing the length of the full message.
	//Timeout per recv performed in the function. total timeout can be greater.
	//THROWS TimeOut if timeout has been reached.
	//THROWS CommunicationException
	std::vector<byte> receiveFromServer(unsigned int timeoutMilli = WSA_INFINITE);
};

