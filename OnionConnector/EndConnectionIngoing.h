#pragma once
#include "InterThreadMsg.h"
class EndConnectionIngoing :public InterThreadMsg 
{
	virtual std::string getType() {
		return "EndConnectionIngoing";
	}
	virtual ~EndConnectionIngoing() {};
};

