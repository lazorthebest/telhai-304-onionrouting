#pragma once
#include "InterThreadMsg.h"
class ConnTerminated :
	public InterThreadMsg
{
	uint64_t _id;
public:
	ConnTerminated(uint64_t id) {
		_id = id;
	}
	uint64_t getId() { return _id; };
	virtual std::string getType() { return "ConnTerminated"; };
	virtual ~ConnTerminated() {};
};
