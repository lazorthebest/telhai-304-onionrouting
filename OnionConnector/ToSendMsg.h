#pragma once
#include <vector>
#include "InterThreadMsg.h"
typedef unsigned char byte;
class ToSendMsg : public InterThreadMsg
{
	std::vector<byte> _msg;
public:
	ToSendMsg(std::vector<byte> msg) {
		_msg = msg;
	}
	virtual std::string getType() {
		return "ToSendMsg";
	}
	std::vector<byte> getMsg() {
		return _msg;
	}
	virtual ~ToSendMsg() {};
};

