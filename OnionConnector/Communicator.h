#pragma once
#include <mutex>
#include <queue>
#include <unordered_map>
#include <ctime>
#include <atomic>
#include "ICommunicator.h"
#include "UnprocessedMsg.h"
#include "ReceivedMsg.h"
#include <array>
#include "EncryptionUtils.h"
#define PORT_LEN 2
#define MSG_LEN 512
#define DEFAULT_PORT 8012
#define PART_LOC_BYTE_LOC 3
#define SEC_IN_MILLI 1000
#define SEC_IN_MICRO 1000000
#define SECONDS_TO_WAIT 30
#define HEADERS_LEN_START 4
#define HEADERS_LEN_LATE 1
class UnprocessedMsg;
enum class NODE_NAMES {
	GUARD_NODE = 0,
	MID_NODE,
	EXIT_NODE,
	NO_NODE,//init value
};

enum class HEADERS {
	BITS_HEADER,
	MSG_TYPE_HEADER,
	LAST_FRAG_LEN//only in the first msg.
};
enum class BITS_HEADER {
	FINAL_FRAGMENT_FLAG,
	IS_GUARD,
	IS_MID
};
enum class MSG_TYPES{
	REGULAR,
	CHECK_NEXT,
	CONN_CLOSURE,
	PATH_GENERATION,
	CREATE_CONN,
	SERVER_CRASH,
	PATH_CLOSURE,
	KEY_EXCHANGE
};


class Communicator: public ICommunicator
{
	std::thread* _recvThread;

	std::atomic<bool> _toShutDown = false;

	std::condition_variable& _outQueueCv;
	std::mutex& _outQueueMtx;
	std::queue<InterThreadMsg *>& _outQueue;//Unprocessed or one of uninit conn/Comm exc exceptions.
	//THROWS CommunicationException and Unitialized conneciton!
	std::vector<byte> receiveFromServer();
	inline bool isLastPart(std::vector<byte> msg);
	inline NODE_NAMES getOrigin(std::vector<byte> msg);
	//deep copy is not an option due to mutex and shallow copy should not be allowed as is bad use.
	Communicator(const Communicator& cm) = delete;
	//DO NOT USE THIS.
	//deep copy is not an option due to mutex and shallow copy should not be allowed as is bad use.
	Communicator& operator=(const Communicator&) = delete; 

	//recived continuosly. Desinged to work as a seperate recieving thread.
	//uses the queue and mutex given to the ctor. It will enter there
	//SENDS ERRORS VIA THE SHARED QUEUE.
	//SENDS InterThreadCommunicationException and InterThreadUnitializedConneciton!
	//Considers 2nd or 3rd node crashes as RecivedMsgs.
	void receiveThread();
public:
	Communicator(std::mutex& outQueueMtx,
		std::condition_variable& _outQueueCv,
		std::queue<InterThreadMsg*>& outQueue);
	virtual ~Communicator();


	//THROWS CommunicationException!
	//calling this when already connected is not allowed!
	virtual void connectToServer();//used to connect to the server. must be used before send and recieve.
	void setCommunicationDetails(std::vector<byte> ip);

	//THROWS CommunicationException and Unitialized conneciton and InvalidMsgLen!
	//toSend should not contain the full msg's length. it will be calculated in the fucntion and fragmented there.
	//NOT MULTITHREADING SAFE, do not call this function simultanously on 2 threads. use lck, mutex or something else.
	//byte flag allows the caller to set the 2nd and 3rd flags (is a msg to check the conn, is a msg to end a connection). 
	//they are located at the 2nd and 3rd bytes respectively.
	virtual void sendToServer( std::vector<byte> toSend, byte paddingLen, MSG_TYPES msgType = MSG_TYPES::REGULAR, NODE_NAMES dst = NODE_NAMES::EXIT_NODE);

	//shuts down the receive thread and shuts the connection down.
	//will do nothing if there is no connection.
	//MUST NOT BE EXECUTED WHILE CONNECT TO SERVER IS EXECUTING. UNDEFINED BEHAVIOUR!!!
	void closeCommunication();
};

