#include "DBManager.h"


DBManager& DBManager::instance()
{
	static DBManager dbm;
	return dbm;
}

void DBManager::getDSIPs(std::vector<std::vector<std::string>>& DSIPs)
{
	std::unique_lock<std::mutex> dbLock(_openDBMtx);
	char* errMessage = nullptr;
	int res = sqlite3_exec(_m_db, "SELECT * FROM DSIPs;",
		callback, &DSIPs, &errMessage);//gets all the data from the database
	if (res != SQLITE_OK) {
		std::string toThrow(errMessage);
		sqlite3_free(errMessage);
		throw SqliteFailureExcpetion(toThrow.c_str());
	}
	dbLock.unlock();
}

void DBManager::keepGuardNodesUpdatedThread()
{
	std::unique_lock<std::mutex> lck(_stopThreadMtx);
	while (!_stopThreadCv.wait_for(lck, std::chrono::hours(24), [this] {return _stopThread; })) {
		runLine("DELETE FROM GuardIPs WHERE Date('now') > Date(ExpirationDate);");//deleting all guards where the expiration date is larger than the date right now
		updateGuardNodes();
	} 
}

void DBManager::updateGuardNodes()
{
	char* errMessage = nullptr;
	std::vector<std::vector<std::string>> tempData;
	std::unique_lock<std::mutex> dbLock(_openDBMtx);
	int res = sqlite3_exec(_m_db, "SELECT * FROM GuardIPs;",
		callback, &tempData, &errMessage);//gets all the data from the database
	dbLock.unlock();
	//Getting a all of the DSIPs, so as to prevent a situation where a fucntion in this will need to call DB to get the DSIP, creating a circular dependancy.
	std::vector<std::vector<std::string>> DSIPsStr;
	byteVec temp;
	std::vector<byteVec> DSIPs;
	getDSIPs(DSIPsStr);
	for (unsigned int i = 0; i < DSIPsStr.size(); i++) {
		std::vector<std::string> IPStr = Utilities::split(tempData[i][0], ".");
		std::transform(IPStr.begin(), IPStr.end(), std::back_inserter(temp),
			[](const std::string& str) { return std::stoi(str); });
		DSIPs.push_back(temp);
	}
	if (res != SQLITE_OK) {
		std::string toThrow(errMessage);
		sqlite3_free(errMessage);
		throw SqliteFailureExcpetion(toThrow.c_str());
	}
	for (size_t i = 0; i < tempData.size(); i++) {//changes data to easier format, and adds to guardData
		std::vector<std::string> date = Utilities::split(tempData[i][1], "-"), guardIPStr = Utilities::split(tempData[i][0], ".");
		Date expirationDate = { stoi(date[2]), stoi(date[1]), stoi(date[0]) };
		Date currDate = Date::getCurrentDate();
		if (expirationDate < currDate) {//date has expired
			removeGuardNode(tempData[i][0]);
			continue;
		}
		std::vector<byte> guardIP;
		std::transform(guardIPStr.begin(), guardIPStr.end(), std::back_inserter(guardIP),
			[](const std::string& str) { return std::stoi(str); });
		std::unique_lock<std::mutex> guardDataLock(_guardDataMtx);
		_guardData.push_back(std::make_pair(expirationDate, guardIP));
	}
	std::vector<byteVec> guardIPs;
	std::transform(_guardData.begin(), _guardData.end(), std::back_inserter(guardIPs), [](std::pair<Date, byteVec> ip) {return ip.second; });

	for (size_t i = 0; i < NUM_GUARDS - _guardData.size(); i++) {//getting new guard nodes as needed from DS
 		PathGenerator& pg = PathGenerator::instance(DSIPs);

		try {
			std::vector<byte> guardIP = pg.getGuardFromDS(guardIPs,DSIPs);//preventing the function from attempting to use the db
			if (guardIP == std::vector<byte>({127, 0, 0, 1}) && std::find(guardIPs.begin(), guardIPs.end(), guardIP) != guardIPs.end()) {//for testing, stops the loop if the request returned localhost and localhost already exists in the db
				break;
			}
			addGuardNode(Utilities::join(guardIP, "."));
			guardIPs.push_back(guardIP);
		}
		catch (NoGuardsAvailException e) {
			break;
		}
	}
}

void DBManager::runLine(std::string command)
{
	char* errMessage = nullptr;
	std::unique_lock<std::mutex> dbLock(_openDBMtx);
	int res = sqlite3_exec(_m_db, command.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK) {
		std::string toThrow(errMessage);
		sqlite3_free(errMessage);
		throw SqliteFailureExcpetion(toThrow.c_str());
	}
	dbLock.unlock();
}

bool DBManager::isValidIP(std::vector<std::string> IP)
{
	return IP.size() != 4 || std::find_if(IP.begin(), IP.end(), [](std::string byteStr) {
		try {
			return std::stoi(byteStr) > 255;
		}
		catch (std::invalid_argument) {
			return true;
		}
		}) != IP.end();
	
}

DBManager::DBManager()
{
	int doesFileExist = _access("ClientDB.sqlite", 0);//checks if the file exists
	std::unique_lock<std::mutex> dbLock(_openDBMtx);
	int res = sqlite3_open("ClientDB.sqlite", &_m_db);//opening/creating the database
	dbLock.unlock();
	if (res != SQLITE_OK) {
		_m_db = nullptr;
		throw CloseDBException();
	}
	if (NOT_EXIST == doesFileExist) {
		std::vector<std::string> commands{ "CREATE TABLE GuardIPs ( \
				  GuardIP TEXT PRIMARY KEY, \
				  ExpirationDate TEXT NOT NULL);",
				  "CREATE TABLE DSIPs ( \
				  DSIP TEXT PRIMARY KEY);" };
		for (std::string command : commands) {
			runLine(command);
		}
	}
	updateGuardNodes();
	_keepGuardNodesUpdatedThread = new std::thread(&DBManager::keepGuardNodesUpdatedThread, this);
}

DBManager::~DBManager()
{
	_stopThread = true;
	_stopThreadCv.notify_all();
	_keepGuardNodesUpdatedThread->join();
	delete _keepGuardNodesUpdatedThread;
	sqlite3_close(_m_db);
}

void DBManager::addGuardNode(std::string guardIP)
{
	std::vector<std::string> guardIPVecStr = Utilities::split(guardIP, ".");
	if (isValidIP(guardIPVecStr)) {
		throw MiscException("Input is not valid IP");
	}
	char buffer[80];
	auto currTime = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now() + std::chrono::hours(2160));//gets the date in 3 months
	tm lt;
	localtime_s(&lt, &currTime);
	strftime(buffer, sizeof(buffer), "%Y-%m-%d", &lt);
	runLine(std::string("INSERT INTO GuardIPs (GuardIP, ExpirationDate) VALUES ('") +
		guardIP + "', '" + buffer + "');");
	Date temp = Date::getCurrentDate();
	std::vector<byte> guardIPVec;
	temp.year += (temp.month + 3) / 12;
	temp.month = (temp.month + 3) % 12;
	
	std::transform(guardIPVecStr.begin(), guardIPVecStr.end(), std::back_inserter(guardIPVec),
		[](const std::string& str) { return std::stoi(str); });
	std::unique_lock<std::mutex> guardDataLock(_guardDataMtx);
	_guardData.push_back({ temp, guardIPVec });
}

std::pair<Date, std::vector<byte>> DBManager::getGuardNode()
{
	std::random_device dev;
	std::mt19937 rng(dev());
	std::unique_lock<std::mutex> guardDataLock(_guardDataMtx);
	std::vector<std::pair<Date, byteVec>> guardDataTemp = _guardData;
	guardDataLock.unlock();
	if (guardDataTemp.size() != 1) {
		auto toErase = std::find_if(guardDataTemp.begin(), guardDataTemp.end(), [this](std::pair<Date, byteVec> pair) {return _lastGuardNodeUsed == pair.second; });
		if (toErase != guardDataTemp.end()) {
			guardDataTemp.erase(toErase);
		}
	}
	std::uniform_int_distribution<std::mt19937::result_type> dist6(0, (unsigned int)guardDataTemp.size() - 1);//gets a random index in guardData
	unsigned int res = dist6(rng);
	_lastGuardNodeUsed = guardDataTemp[res].second;
	return guardDataTemp[res];
}

std::vector<byte> DBManager::getDSIP()
{
	std::vector<std::vector<std::string>> tempData;
	getDSIPs(tempData);
	std::random_device dev;
	std::mt19937 rng(dev());
	std::uniform_int_distribution<std::mt19937::result_type> dist6(0, tempData.size() - 1);//gets a random index in guardData
	std::vector<std::string> IPStr = Utilities::split(tempData[dist6(rng)][0], ".");
	std::vector<byte> IP;
	std::transform(IPStr.begin(), IPStr.end(), std::back_inserter(IP),
		[](const std::string& str) { return std::stoi(str); });
	return IP;
}

void DBManager::addDSIP(std::string DSIP)
{
	if (isValidIP(Utilities::split(DSIP, "."))) {
		throw MiscException("Input is not valid IP");
	}
	runLine(std::string("INSERT INTO DSIPs (DSIP) VALUES ('") +
		DSIP + "');");
}

void DBManager::removeGuardNode(std::string GuardIP)
{
	runLine((std::string("DELETE FROM GuardIPs WHERE GuardIP='") + GuardIP + "';").c_str());
}

void DBManager::removeDSIP(std::string DSIP)
{
	runLine((std::string("DELETE FROM DSIPs WHERE DSIP='") + DSIP + "';").c_str());
}

bool operator>(const Date& d1, const Date& d2)
{
	if (d1.year == d2.year) {//checks year, then month, then day
		if (d1.month == d2.month) {
			if (d1.day == d2.day) {
				false;
			}
			else return d1.day > d2.day;
		}
		else return d1.month > d2.month;
	}
	return d1.year > d2.year;
}

bool operator<(const Date& d1, const Date& d2)
{
	if (d1.year == d2.year) {//checks year, then month, then day
		if (d1.month == d2.month) {
			if (d1.day == d2.day) {
				false;
			}
			else return d1.day < d2.day;
		}
		else return d1.month < d2.month;
	}
	return d1.year < d2.year;
}

int callback(void* data, int argc, char** argv, char** azColName)
{
	std::vector<std::vector<std::string>>* allContents = static_cast<std::vector<std::vector<std::string>>*>(data);
	std::vector<std::string> temp;
	for (int i = 0; i < argc; i++) {
		temp.push_back(argv[i]);
	}
	allContents->push_back(temp);
	return 0;
}

Date Date::getCurrentDate()
{
	Date temp;
	char buffer[5];
	auto currTime = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());//gets the current time
	tm lt;
	localtime_s(&lt, &currTime);
	strftime(buffer, sizeof(buffer), "%d", &lt);
	temp.day = std::stoi(buffer);
	strftime(buffer, sizeof(buffer), "%m", &lt);
	temp.month = std::stoi(buffer);
	strftime(buffer, sizeof(buffer), "%Y", &lt);
	temp.year = std::stoi(buffer);
	return temp;
}
