#include "Utilities.h"

std::vector<std::string> Utilities::split(std::string s, std::string delimeter)
{
	size_t pos = 0;
	std::vector<std::string> toReturn;
	while ((pos = s.find(delimeter)) != std::string::npos) {//finds the next index of delimeter, get a substring until it, then erases said substring as well as the delimeter
		toReturn.push_back(s.substr(0, pos));
		s.erase(0, pos + delimeter.length());
	}
	toReturn.push_back(s);
	return toReturn;
}

byte Utilities::isolateBit(byte aByte, byte byteToIsolate)
{
	aByte = (aByte << (BITS_FROM_SIGNIFIANT_TO_LEAST - byteToIsolate));
	return aByte>>BITS_FROM_SIGNIFIANT_TO_LEAST;
}

byte Utilities::zeroBit(byte aByte, byte bitToZero)
{
	byte leftSide = 0;
	byte rightSide = 0;
	leftSide = (aByte >> (bitToZero + 1));
	leftSide = leftSide<< (bitToZero + 1);
	rightSide = (aByte << (BITS_FROM_SIGNIFIANT_TO_LEAST - bitToZero  + 1));
	rightSide = rightSide>>(BITS_FROM_SIGNIFIANT_TO_LEAST - bitToZero + 1);
	aByte = leftSide + rightSide;
	return aByte;
}

bool Utilities::isIPV4InternetConnectionOk()
{
	CoInitialize(NULL);//ensuring there is an internet connection
	{
		CComPtr<INetworkListManager> pNLM;
		HRESULT hr = CoCreateInstance(CLSID_NetworkListManager, NULL,
			CLSCTX_ALL, __uuidof(INetworkListManager), (LPVOID*)&pNLM);
		if (SUCCEEDED(hr))
		{
			NLM_CONNECTIVITY con = NLM_CONNECTIVITY::NLM_CONNECTIVITY_DISCONNECTED;
			hr = pNLM->GetConnectivity(&con);
			if SUCCEEDED(hr)
			{
				if (con & NLM_CONNECTIVITY_IPV4_INTERNET) {
					return true;
				}
			}
		}
	}
	CoUninitialize();
	return false;
}

std::string Utilities::join(std::vector<byte> vec, std::string delimeter) {
	std::string toReturn;
	for (byte part : vec) {
		toReturn += std::to_string(part) + delimeter;
	}
	return toReturn.substr(0,toReturn.length() - 1);
}
