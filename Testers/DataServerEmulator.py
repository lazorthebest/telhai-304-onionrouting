import socket
import threading
import time
from colorama import Fore
from binascii import hexlify
DS_PORT = 43984
OR_PORT = 8012
BUFFER_SIZE = 2048
FLAG_BYTE = 2
DATA_FLAG_BYTE = 4

GIVE_NODES = 0
GET_NEW_NODE = 1
REPORT_NODE = 2
GET_GUARD_NODE = 3
NODE_ALIVE = 4

conn_id = 0

class NODE_MSGS:
	GIVE_TOKEN_AND_CONNECT = 0
	GIVE_TOKEN_AND_CONNECT_EXIT = 1
	RECONNECT_TO_NODE = 2
	FORWARD_AS_MSG_TO_GUARD = 3

class NODE_MSGS_RESPONSES:
	GIVE_TOKEN_AND_CONNECT = 0
	GIVE_TOKEN_AND_CONNECT_EXIT = 1
	RECONNECT_TO_NODE = 2
	FAILED_TO_RECONNECT = 3


GIVE_TOKEN_AND_CONNECT_RES = 0
msgId = 0

token_lck = threading.Lock()
tokens = 0

router_lck = threading.Lock()
routers = []

def recvDS(socket, size=None):
	if size == None:
		buff = socket.recv(2)
		size = int.from_bytes(buff, "big")
	to_return = b""
	buff = b""
	while len(to_return) < size:
		buff = socket.recv(2048)
		to_return += buff
	return to_return

def basicNodeRecv(socket, size=512):
	to_return = b""
	to_return += socket.recv(size)
	return to_return

# len1, len2, part_loc, flags, x, x, port1, port2
# flags:is last part of msg, server crash msg, connection closure, direct message to guard
def sendDS(socket_to_send, msg):
	size = int.to_bytes(len(msg), 2, "big")
	socket_to_send.send(size)
	socket_to_send.send(msg)


def create_ips():
	while (len(routers) == 0):
		time.sleep(0.1)
	ip = [127,0,0,1]
	global tokens
	token_lck.acquire()
	router_lck.acquire()
	tokens += 1
	to_return = ip + (list(tokens.to_bytes(8, 'big') * 2))
	sendDS(routers[0], tokens.to_bytes(8, 'big') * 2)
	tokens += 1
	to_return += ip + (list(tokens.to_bytes(8, 'big') * 2))
	sendDS(routers[0], tokens.to_bytes(8, 'big') * 2)
	router_lck.release()
	token_lck.release()

	return to_return

def DSThread():
	threads = []
	while True:
		data_server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		data_server_socket.bind(('localhost', DS_PORT))
		data_server_socket.listen()
		(client_socket, address) = data_server_socket.accept()
		threads.append(threading.Thread(target = handleOCThread, args=(client_socket,routers)))
		threads[-1].start()

def handleOCThread(client_socket, routers):
	global tokens
	while True:
		try:
			client_msg = recvDS(client_socket)
		except ConnectionResetError:
			return
		if len(client_msg) == 0: return
		print(Fore.BLUE + "DS: received msg")
		print(hexlify(client_msg))
		if client_msg[0] == GIVE_NODES:
			print(Fore.CYAN+ "DS: GIVE NODES")
			ips = create_ips()
			sendDS(client_socket, bytes(ips))
		elif client_msg[0] == GET_NEW_NODE:
			print(Fore.CYAN + "DS: GET NEW NODE")
			token_lck.acquire()
			tokens += 1
			token = tokens
			token_lck.release()
			sendDS(client_socket, bytes([127,0,0,1] + list(token.to_bytes(8, "big"))))
		elif client_msg[0] == REPORT_NODE:
			print(Fore.CYAN + "DS: REPORT NODE")
			sendDS(client_socket, bytes([0]))
		elif client_msg[0] == GET_GUARD_NODE:
			print(Fore.CYAN + "DS: GET GUARD NODE")
			sendDS(client_socket, bytes([True] + [127,0,0,1]))
		elif client_msg[0] == NODE_ALIVE:
			sendDS(client_socket, bytes(True))
			print(Fore.CYAN + "DS: NODE ALIVE")
			router_lck.acquire()
			routers.append(client_socket)
			router_lck.release()
			return

def make_long_msg(n):
	to_return = b""
	for i in range(n):
		to_return += bytes(str(i), "utf8") + b" "
	return to_return[:-1] + b"\n"

def server_thread():

	server_socket = socket.socket(
		socket.AF_INET, socket.SOCK_STREAM)
	# bind the socket to a public host,
	# and a well-known port
	server_socket.bind(('localhost', 80))
	# become a server socket
	server_socket.listen(5)
	while True:
		client = server_socket.accept()[0]

		client.send(make_long_msg(100))


		print(client.recv(2905))

		client.shutdown(socket.SHUT_WR)
		client.recv(512)
		client.close()
		print("FINISHED")


def main():
	DS = threading.Thread(target = DSThread)
	DS.start()
	server = threading.Thread(target = server_thread)
	server.start()
	DS.join()
	server.join()




if __name__ == "__main__":
	main()