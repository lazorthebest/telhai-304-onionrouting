import socket
import time
import random
import threading

MSG_SIZE = 512
HEADER_LEN = 2
FIRST_HEADER_LEN = 4
FOLLOWING_HEADER_LEN = 1
SIZE_LEN = 2
NUM_SEND = 100000
NUM_TIMES = 1
DS_PORT = 43984
OR_PORT = 8012
N = 20
MAX = 10

socks = []

def set_bit(v, index, x):
    mask = 1 << index   # Compute mask, an integer with just bit 'index' set.
    v &= ~mask          # Clear the bit indicated by the mask (if x is False)
    if x:
        v |= mask         # If x was True, set the bit indicated by the mask.
    return v            # Return the result, we're done.


def send_msg(s, msg, flags=0, msg_type=0):
    if type(msg) != bytes:
        msg = bytes(msg, "utf8")

    if len(msg) > MSG_SIZE - FIRST_HEADER_LEN:  # if the msg will need to be sent in multiple packets
        len_last_frag = (len(msg) - (MSG_SIZE - FIRST_HEADER_LEN)) % (MSG_SIZE - FOLLOWING_HEADER_LEN) + \
                        FOLLOWING_HEADER_LEN
        if len_last_frag == 1:
            len_last_frag = MSG_SIZE
        headers = int.to_bytes(flags, 1, "big") + int.to_bytes(msg_type, 1, "big") + \
            int.to_bytes(len_last_frag, 2, "big")
        s.send(headers + msg[:MSG_SIZE - FIRST_HEADER_LEN])
    else:
        headers = int.to_bytes(flags + 1, 1, "big") + int.to_bytes(msg_type, 1, "big") + \
              int.to_bytes(len(msg) + FIRST_HEADER_LEN, 2, "big")
        msg += b"\x00" * (MSG_SIZE - (len(msg) + len(headers)))  # making msg be of size MSG_SIZE
        s.send(headers + msg)
        return

    i = MSG_SIZE - FIRST_HEADER_LEN
    headers = int.to_bytes(flags, 1, "big")

    while len(msg) > (i + 1) * (MSG_SIZE - FOLLOWING_HEADER_LEN):
        s.send(headers + msg[i * (MSG_SIZE - FOLLOWING_HEADER_LEN):(i + 1) * (MSG_SIZE - FOLLOWING_HEADER_LEN)])
        i += 1

    headers = int.to_bytes(flags + 1, 1, "big")
    msg += b"\x00" * (MSG_SIZE - (len(msg) + len(headers)))  # making msg be of size MSG_SIZE

    s.send(headers + msg[i * (MSG_SIZE - FOLLOWING_HEADER_LEN)])


def recv_msg(s):
    msg = b""
    buff = b""
    first_run = True
    while first_run or buff[0] % 2 != 1:
        first_run = False
        buff = s.recv(MSG_SIZE)
        print(buff)
        if len(msg) == 0:
            flags = buff[:FIRST_HEADER_LEN]
            last_frag_len = int.from_bytes(flags[HEADER_LEN:FIRST_HEADER_LEN], "big")
        if buff[0] % 2 == 1:
            buff = buff[:last_frag_len]
        if len(msg) == 0:
            msg += buff[FIRST_HEADER_LEN:]
        else:
            msg += buff[FOLLOWING_HEADER_LEN:]

    return flags, msg


def start_path():
    s = socket.socket(
        socket.AF_INET, socket.SOCK_STREAM)
    while True:
        try:
            s.connect(("localhost", OR_PORT))
            break
        except ConnectionRefusedError:
            pass
    print("IN")
    # send_msg(s, b"", set_bit(0, 1, True), 3)
    # recv_msg(s)
    # print("IN")
    # send_msg(s, bytes([127, 0, 0, 1]), set_bit(0, 1, True), 3)
    # if recv_msg(s)[0] == 0:
    #     raise Exception("Error occurred")
    # print("IN")
    #
    token = bytes([random.randint(0, 255) for _ in range(16)])  # coordinating with second node
    to_return = token
    socks[random.randint(0, len(socks) - 1)].send(int.to_bytes(len(token), 2, "big") + token)
    send_msg(s, token[:len(token) // 2], set_bit(0, 2, True), 3)
    print("HERE")
    if recv_msg(s)[1][1:] != token[len(token) // 2:]:
        raise Exception("Incorrect return value")
    print("IN")
    send_msg(s, bytes([127, 0, 0, 1]), set_bit(0, 2, True), 3)
    if recv_msg(s)[0] == 0:
        raise Exception("Error occurred")
    print("IN")

    token = bytes([random.randint(0, 255) for _ in range(16)])  # coordinating with third node
    socks[random.randint(0, len(socks) - 1)].send(int.to_bytes(len(token), 2, "big") + token)
    send_msg(s, token[:len(token) // 2], 0, 3)
    if recv_msg(s)[1][1:] != token[len(token) // 2:]:
        raise Exception("Incorrect return value")
    print("IN")

    return s#, to_return


def path_closure(s):
    send_msg(s, "", 0, 6)
    print(recv_msg(s))


def reconnect_node():
    s = socket.socket(
        socket.AF_INET, socket.SOCK_STREAM)
    s.connect(("localhost", OR_PORT))
    send_msg(s, int.to_bytes(1, 1, "big"), 3)
    recv_msg(s)
    return s


def end_conn(s, conn_id):
    send_msg(s, conn_id, 0, 2)
    print(recv_msg(s))


def check_guard(s):
    send_msg(s, "", 1, 1)
    recv_msg(s)


def new_conn(s, ip, port):
    msg = bytes(ip) + int.to_bytes(port, 2, "big")
    send_msg(s, msg, 0, 4)
    return recv_msg(s)[1][1:9]


def server_conn_thread():
    server_socket = socket.socket(
        socket.AF_INET, socket.SOCK_STREAM)
    # bind the socket to a public host,
    # and a well-known port
    server_socket.bind(('localhost', 1529))
    # become a server socket
    server_socket.listen(5)
    clients = []

    for _ in range(N):
        clients.append(server_socket.accept()[0])

    i = 1
    client = 0
    while i < MAX:
        if i - 1 != int.from_bytes(clients[client].recv(1024), "big"):
            raise Exception("1Bad order at " + str(i))
        clients[client].send(int.to_bytes(i, 2, "big"))
        i += 2
        client += 1
        client %= len(clients)
    print("HERE", i)
    for sock in clients:
        sock.shutdown(socket.SHUT_WR)
        sock.recv(MSG_SIZE)
        sock.close()


def data_server_thread():
    global socks
    data_server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    data_server_socket.bind(('localhost', DS_PORT))
    data_server_socket.listen()
    while True:
        (client_socket, address) = data_server_socket.accept()
        l = int.from_bytes(client_socket.recv(2), "big")
        buff = client_socket.recv(l)
        if buff != b"\x04":
            raise Exception("Didn't get starting message")
        client_socket.send(int.to_bytes(1, 2, "big") + int.to_bytes(4, 1, "big"))  # send message confirming connection
        socks.append(client_socket)




def main():
    a = threading.Thread(target=server_conn_thread)
    a.start()
    conn_ids = []
    print("Start path soon")
    s = start_path()
    print("Path started")
    for _ in range(N):
        conn_ids.append(new_conn(s, [127, 0, 0, 1], 1529))
        # if _ == 0:
        #     send_msg(s, token[:len(token) // 2], set_bit(0, 2, True), 3)
        #     if recv_msg(s)[1][1:] != token[len(token) // 2:]:
        #         raise Exception("Incorrect return value")
        #     print("IN")
        #     send_msg(s, bytes([127, 0, 0, 1]), set_bit(0, 2, True), 3)
        #     if recv_msg(s)[0] == 0:
        #         raise Exception("Error occurred")
        #     print("IN")
    print("new connections")
    i = 0
    conn_id = 0
    while i < MAX:
        send_msg(s, conn_ids[conn_id] + int.to_bytes(i, 2, "big"))
        if i + 1 != int.from_bytes(recv_msg(s)[1][8:], "big"):
            raise Exception("2Bad order at " + str(i))
        i += 2
        conn_id += 1
        conn_id %= len(conn_ids)
    print("sent messages")


    for conn_id in conn_ids:
        end_conn(s, conn_id)

    print("Closed connections")
    path_closure(s)
    print("Closed path")
    print(s.recv(MSG_SIZE))
    s.shutdown(socket.SHUT_WR)
    s.close()
    print("FINISHED")
    a.join()
    return 0


if __name__ == "__main__":
    threading.Thread(target=data_server_thread).start()
    while True:
        main()
        #break
    exit(0)
